<?php 
class OpenFitReportHandler {
    public static function getReportData() {
      global $user;
      // Measurement unit preferences
      $preferences = OpenFitUserSetting::get($user->uid);
      $distance_units = $preferences[OpenFitUserSetting::TYPE_DISTANCE_UNITS];
      $distance_measurement = OpenFitMeasurement::getConversionInfo($distance_units);
      $distance_measurement->unit_decimals = 0;
      $calorie_units = $preferences[OpenFitUserSetting::TYPE_CALORIE_UNITS];
      if ($calorie_units != OpenFitMeasurement::UNITS_CALORIE) {
        $calorie_label = t('Energy');
      } else {
        $calorie_label = t('Calories');
      }
      $speed_units = $preferences[OpenFitUserSetting::TYPE_SPEED_UNITS];
      $time_measurement = array('unit_symbol' => t('h:m:s'), 'time' => true);
      $pace_measurement = OpenFitMeasurement::getConversionInfo($distance_units);
      $pace_measurement->time = true;
      $pace_measurement->unit_symbol = '/' . $pace_measurement->unit_symbol;
      
      $metrics = array(
        'default' => 'distance',
        'items' => array(
          array('id' => 'distance', 'name' => t('Distance'), 'min' => 0, 
            'measurement' => $distance_measurement),
          array('id' => 'duration', 'name' => t('Time'), 'min' => 0, 
            'measurement' => array('unit_symbol' => t('h:m:s'), 'time' => true)),
          array('id' => 'calories', 'name' => $calorie_label, 'min' => 0,
            'measurement' => OpenFitMeasurement::getConversionInfo($calorie_units)),
          array('id' => 'speed', 'name' => t('Avg Speed'), 'min' => 0,
            'measurement' => OpenFitMeasurement::getConversionInfo($speed_units)),
          array('id' => 'pace', 'name' => t('Avg Pace'), 'min' => 0, 'function' => '_getPaceData',
            'measurement' => $pace_measurement),
        ),
      );
      $groupby = array(
        'default' => 'month',
        'items' => array(
          array('id' => 'week', 'name' => t('Week')),
          array('id' => 'month', 'name' => t('Month')),
          array('id' => 'year', 'name' => t('Year')),
        ),
      );
      
      // Weekly data
      $columns = array('distance','duration','calories');
      $summary_rows = array(
        'week' => array(
          'rows' => OpenFitActivity::getWeekStatistics($columns, 25, $user->uid),
          'label_format' => 'j M',
        ),
        'month' => array(
          'rows' => OpenFitActivity::getMonthStatistics($columns, 11, $user->uid),
          'label_format' => 'M Y',
        ),
        'year' => array(
          'rows' => OpenFitActivity::getYearStatistics($columns, 4, $user->uid),
          'label_format' => 'Y',
        ),
      );
      $data = array();
      foreach ($summary_rows as $group => $rows) {
        if (count($rows['rows']) > 0) {
          $group_data = array('labels' => array(), 'data' => array());
          foreach ($rows['rows'] as $row) {
            $label = strtolower($row['group']->format($rows['label_format']));
            $label = str_replace(' ', '<br>', $label);
            $group_data['labels'][] = $label;
            $group_data['data']['distance'][] = $row['distance'];
            $group_data['data']['duration'][] = $row['duration'];
            $group_data['data']['calories'][] = $row['calories'];
            $speed = null;
            $pace = null;
            if (isset($row['distance']) && isset($row['duration']) && $row['duration'] > 0) {
              $speed = $row['distance'] / $row['duration'];
              if ($speed != 0) $pace = 1/$speed;
            }
            $group_data['data']['speed'][] = $speed;
          }
          $data[$group] = $group_data;
        }
      }
      
      $data = array(
        'metrics' => $metrics,
        'groupby' => $groupby,
        'data' => $data,
      );
      die(print_r(json_encode($data), true));
    }
}
?>
