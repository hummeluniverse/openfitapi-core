<?php 
class NodeAccessHandler {
  
  public static function setNodeValue() {
    $params = array_merge($_GET, $_POST); 

    // Gather URL parameters.
    $nid = isset($params['nid']) ? intval($params['nid']) : null;
    $field = isset($params['f']) ? $params['f'] : null;
    $value = isset($params['v']) ? $params['v'] : null;
    
    // Validate parameters and coerce types as needed.
    switch ($field) {
      case 'privacy':
        // TODO: Consolidate this mapping somewhere into openfit Activity class
        switch ($value) {
          case 'friends': $value = 2; break;
          case 'public': $value = 1; break;
          case 'private': $value = 0; break;
          default: $value = null; break;
        }
        break;
      case 'notes':
        break;
      default: $field = null; break;
    }
    if (!isset($nid) || !isset($field) || !isset($value)) throw new Exception(t('Invalid method parameters.'));
    
    // Load the node and perform update access check.
    require_once DRUPAL_ROOT . '/includes/common.inc';
    module_load_include('module', 'node');
    module_load_include('inc', 'field', 'field.attach');
    module_load_include('module', 'user');
    
    $node = node_load($nid);
    if (!node_access('update', $node)) throw new Exception('User does not have access to nid ' . $nid, 403);
  
    // Set the appropriate node fields.
    $modified = false;
    switch ($field) {
      case 'privacy':
        $node->status = $value == 1 ? 1 : 0;
        $modified = true;
        break;
    }
    // Save the node.
    if ($modified) node_save($node);
    
    // Set the appropriate activity fields.
    $fields = null;
    switch ($field) {
      case 'privacy':
      case 'notes':
        $fields = array("activity_$field" => $value);
        break;
    }
   
    if (isset($fields)) {
      module_load_include('inc', 'openfit_api', 'openfit_api.Activity');
      $activity= OpenFitActivity::getActivity($node->nid);
      if (isset($activity)) {
        OpenFitActivity::updateActivityRecords($activity->activity_id, $fields);
      }
    }
    
    die('ok');
  }
}
?>
