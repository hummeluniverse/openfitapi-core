﻿<?php
require_once('openfit_api.ActivityDataTrack.inc');

class GpxFileImporter {
  public function __construct() {
  }

  public function importFile($filename) {
    $time_data_format = ActivityDataTrackAccess::getTimeDataFormat($this->trackVersion);
    $this->locationDataFormat = $time_data_format . ActivityDataTrackAccess::getDataFormat(ActivityDataTrackAccess::LOCATION);
    $this->elevationDataFormat = $time_data_format . ActivityDataTrackAccess::getDataFormat(ActivityDataTrackAccess::ELEVATION);
    
    $import_results = array();
    // Set the import metadata.
    $import_results['metadata'] = array(
      'type' => 'file',
      'file' => array('name' => $filename),
    );

    $this->readFile($filename);
    
    $import_results['activities'] = $this->activities;
    
    return $import_results;
  }
  
  private function readFile($filename) {
    $this->reader = new XMLReader();  
    if (!$this->reader->open($filename)) return;
    
    while($this->reader->read()) { 
      if ($this->reader->nodeType == XmlReader::WHITESPACE || $this->reader->nodeType == XmlReader::SIGNIFICANT_WHITESPACE) continue;
      
      switch ($this->reader->nodeType) {
        case XmlReader::ELEMENT:
          $this->startElement();
          if ($this->reader->isEmptyElement) $this->endElement();  
          break;
          
        case XmlReader::END_ELEMENT:
          if ($this->elements[count($this->elements)-1] != $this->reader->localName) throw new Exception('Unexpected element: ' . $this->reader->localName);
          $this->endElement();
          break;
          
        case XmlReader::TEXT:
          if ($this->reader->hasValue) $this->characters();
          break;
      }
    }
    $this->reader->close(); 
    $this->reader = null;
  }
  
  private function startElement() {
    $this->parent = $this->element;
    $this->element = $this->reader->localName;
    $this->elements[] = $this->element;
    
    switch ($this->element) {
      case 'trk':
        $this->createActivity();
        break;
      case 'trkpt':
        if ($this->parent == 'trkseg') {
          $this->trackPoint = array();
          $this->trackPoint['lat'] = floatval($this->reader->getAttribute('lat'));
          $this->trackPoint['lon'] = floatval($this->reader->getAttribute('lon'));
        }
        break;
    }
  }
  
  private function endElement() {
    switch ($this->element) {
      case 'trk':
        if (isset($this->activity['start_time'])) {
          $this->activity['start_time'] = $this->activity['start_time']->format('Y-m-d H:i:s');
          if (isset($this->locationTrack)) {
            $this->activity['data_tracks'][ActivityDataTrackAccess::LOCATION] = $this->locationTrack;
          }
          if (isset($this->elevationTrack)) {
            $this->activity['data_tracks'][ActivityDataTrackAccess::ELEVATION] = $this->elevationTrack;
          }
          $this->activities[] = $this->activity;
        }
        $this->activity = null;
        break;
      case 'trkseg':
        $this->segmentEndTime = $this->lastPointTime;
        break;
      case 'trkpt':
        if ($this->parent == 'trkseg') {
          if (isset($this->trackPoint['time'])) {
            if (isset($this->segmentEndTime)) {
              $from = $this->segmentEndTime->diff($this->activity['start_time'], true);
              $from = (($from->days * 24 + $from->h) * 60 + $from->i) * 60 + $from->s;
              $to = $this->trackPoint['time']->diff($this->activity['start_time'], true);
              $to = (($to->days * 24 + $to->h) * 60 + $to->i) * 60 + $to->s;
              $this->activity['timer_stops'][] = array('from' => $from, 'to' => $to);
            }
            $this->segmentEndTime = null;
            if (!isset($this->activity['start_time'])) $this->activity['start_time'] = $this->trackPoint['time'];
            
            if (!isset($this->locationTrack)) {
              $this->locationTrack = pack('S', $this->trackVersion);
            }
            if (isset($this->trackPoint['ele'])) {
              if (!isset($this->elevationTrack)) {
                $this->elevationTrack = pack('S', $this->trackVersion);
              }
            }
            $offset = $this->trackPoint['time']->diff($this->activity['start_time'], true);
            $offset = (($offset->days * 24 + $offset->h) * 60 + $offset->i) * 60 + $offset->s;
            if ($offset > 65000 && $this->trackVersion == 1) {
              $this->convertTracksVersion();
            }
            $this->locationTrack .= pack($this->locationDataFormat, $offset, $this->trackPoint['lat'], $this->trackPoint['lon']);            
            if (isset($this->trackPoint['ele'])) {
              $this->elevationTrack .= pack($this->elevationDataFormat, $offset, $this->trackPoint['ele']);
            }
            $this->lastPointTime = $this->trackPoint['time'];
          }
          $this->trackPoint = array();
        }
        break;
    }
    
    array_pop($this->elements);
    $this->element = $this->parent;
    $this->parent = count($this->elements) > 1 ? $this->elements[count($this->elements)-2] : null;    
  }
  
  /**
   * Called when the elapsed time can no longer fit in 2 bytes (> ~18 hours).
   * Convert existing elevation & data tracks to version 0 which stores elapsed time in 4 bytes.
   */
  private function convertTracksVersion() {
    $this->trackVersion = 0; 
    $time_data_format = ActivityDataTrackAccess::getTimeDataFormat($this->trackVersion);
    $this->locationDataFormat = $time_data_format . ActivityDataTrackAccess::getDataFormat(ActivityDataTrackAccess::LOCATION);
    $this->elevationDataFormat = $time_data_format . ActivityDataTrackAccess::getDataFormat(ActivityDataTrackAccess::ELEVATION);

    $this->locationTrack = $this->convertTrackVersion($this->locationTrack, ActivityDataTrackAccess::LOCATION);
    $this->elevationTrack = $this->convertTrackVersion($this->elevationTrack, ActivityDataTrackAccess::ELEVATION);
  }
  
  private function convertTrackVersion(&$prior_track, $track_type) {
    if (!isset($prior_track) || strlen($prior_track) < 2) return null;
    $track_version = unpack('S', $prior_track);
    $track_version = $track_version[1];
    if ($track_version == $this->trackVersion) return $prior_track;
    
    $prior_time_data_format = ActivityDataTrackAccess::getTimeDataFormat($track_version);
    $prior_time_data_size = ActivityDataTrackAccess::getTimeDataSize($track_version);
    $time_data_format = ActivityDataTrackAccess::getTimeDataFormat($this->trackVersion);
    $data_size = ActivityDataTrackAccess::getDataSize($track_type);
    $i = 2;
    $track_len = strlen($prior_track);
    $out = pack('S', $this->trackVersion);
    while ($i < $track_len) {
      $time = unpack($prior_time_data_format, substr($prior_track, $i, $prior_time_data_size));
      $i += $prior_time_data_size;
      $value = substr($prior_track, $i, $data_size);
      $i += $data_size;
      $out .= pack($time_data_format, $time) . $value;
    }
    return $out;
  }
  
  private function characters() {
    switch ($this->element) {
      case 'name':
        $this->activity['name'] = $this->reader->value;
        break;
      case 'cmt':
        $this->activity['notes'] = $this->reader->value;
        break;
      case 'ele':
        if ($this->parent == 'trkpt') $this->trackPoint['ele'] = floatval($this->reader->value);
        break;
      case 'time':
        if ($this->parent == 'trkpt') $this->trackPoint['time'] = new DateTime($this->reader->value);
        break;
    }
  }
  
  private function createActivity() {
    $this->activity = array();
    $this->activity['data_tracks'] = array();
    $this->activity['timer_stops'] = array();
    $this->trackPoint = array();
    $this->lastPointTime = null;
    $this->segmentEndTime = null;
    $this->locationTrack = null;
    $this->elevationTrack = null;
  }

  private $trackVersion = 1;
  private $locationDataFormat;
  private $elevationDataFormat;
  private $reader = null;
  private $elements = array();
  private $element = null;
  private $parent = null;
  
  private $activity = null;
  private $trackPoint = array();
  private $lastPointTime = null;
  private $segmentEndTime = null;
  private $locationTrack = null;
  private $elevationTrack = null;
  
  private $activities = array();
}
