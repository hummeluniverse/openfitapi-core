<?php

define('OPENFIT_API_VERSION', '1.0');


/**
 * Login user via services.
 * Code copied from _user_resource_login. We add additional info.
 */
function openfit_api_resource_user_login($username, $password) {
  global $user;

  if ($user->uid) {
    // user is already logged in
    return services_error(t('Already logged in as @user.', array('@user' => $user->name)), 406);
  }

  $user = user_load_by_mail($username);
  if (isset($user) && isset($user->uid) && $user->uid != 0) $username = $user->name;
  
  $uid = user_authenticate($username, $password);

  if ($uid) {
    $user = user_load($uid);
    if ($user->uid) {
      user_login_finalize();

      $return = new stdClass();
      $return->sessid = session_id();
      $return->session_name = session_name();

      services_remove_user_data($user);

      $return->user = $user;
      
      if (!empty($user->picture->uri)) {
        $user_picture = $user->picture->uri;
        $style = variable_get('user_picture_style', 'thumbnail');
        $user_picture = image_style_url($style, $user_picture);
      } else {
        $user_picture = variable_get('user_picture_default', '');
        global $base_root; 
        $user_picture = $base_root . '/' . $user_picture;
      }
      
      $settings = OpenFitUserSetting::get($user->uid);
      $settings['picture'] = $user_picture;
      $openfit_info = array(
        'access' => node_access('create', OpenFitActivity::NODE_TYPE_ACTIVITY, $user) ? 'write' : 'read-only',
        'settings' => $settings,
        'version' => OPENFIT_API_VERSION,
      );
      unset($openfit_info['settings']['timezone']);
      unset($openfit_info['settings']['language']);
      $return->user->openfit_info = (object)$openfit_info;

      return $return;
    }
  }
  watchdog('user', 'Invalid login attempt for %username.', array('%username' => $username));
  return services_error(t('Wrong username or password.'), 401);
}

/**
 * Access check for services retrieve callback.
 *
 * @param $op
 *   Access operator type (ex: 'view')
 * @param $args
 *   Access check arguments. [0] is the node id.
 */
function openfit_api_resource_activity_retrieve_access($op, $args) {
  global $user;
  $nid = $args[0];
  $node = node_load($nid);
  return node_access($op, $node, $user);
}

// TODO: Consolidate this mapping somewhere into openfit Activity class
function openfit_api_resource_activity_statuses() {
  return array(
    0 => 'private',
    1 => 'public',
    2 => 'friends',
  );
}

/**
 * Return a specific activity node.
 *
 * @param $nid
 *   The node id.
 */
function openfit_api_resource_activity_retrieve($nid) {
  $node = node_load($nid);
  if (!node_access('view', $node)) return null;
  
  $activity = OpenFitActivity::getActivity($nid);
  
  if (isset($activity)) {
    $node->openfit_info['activity'] = $activity;
    $node->openfit_info['laps'] = OpenFitActivity::getActivityLaps($node->openfit_info['activity']->activity_id);
  }
  
  $row = $node->openfit_info['activity'];
  
  $utc_timezone = new DateTimeZone('UTC');

  $activity_localtime = new DateTime($row->activity_start, $utc_timezone);
  $activity_timezone = $row->activity_timezone;
  $activity_iso_timezone = OpenFitMeasurementDateTime::getISOTimezone($activity_timezone);
  OpenFitMeasurementDateTime::offsetDate($activity_localtime, $activity_timezone);
  $start_time = $activity_localtime->format('Y-m-d') . 'T' . $activity_localtime->format('H:i:s') . $activity_iso_timezone;
  $uri = openfit_api_resource_activity_resource_uri(array('activity', $nid));
  $privacy = 'private';
  $statuses = openfit_api_resource_activity_statuses();
  if (isset($statuses[$row->activity_privacy])) $privacy = $statuses[$row->activity_privacy];
  $item = array(
    'start_time' => $start_time,
    'timezone' => floatval($activity_timezone),
    'privacy' => $privacy,
    'total_distance' => floatval($row->activity_distance),
    'duration' => floatval($row->activity_duration),
    'clock_duration' => floatval($row->activity_clock_duration),
    'type' => $row->category_name,
    'name' => $row->activity_name,
    'calories' => floatval($row->activity_calories),
    'user_id' => intval($row->uid),
    'uri' => $uri,
    'activity' => url(drupal_get_path_alias('node/' . $nid), array('absolute' => TRUE)),
  );
  if (isset($row->activity_location) && strlen($row->activity_location) > 0) $item['location_name'] = $row->activity_location;
  if (isset($row->activity_notes) && strlen($row->activity_notes) > 0) $item['notes'] = $row->activity_notes;
  if (isset($row->activity_elevation_gain)) $item['elevation_gain'] = floatval($row->activity_elevation_gain);
  if (isset($row->activity_elevation_loss)) $item['elevation_loss'] = floatval($row->activity_elevation_loss);
  if (isset($row->speed)) $item['avg_speed'] = round($row->speed, 3);
  if (isset($row->activity_max_speed)) $item['max_speed'] = round($row->activity_max_speed, 3);
  if (isset($row->activity_avg_heartrate)) $item['avg_heartrate'] = floatval($row->activity_avg_heartrate);
  if (isset($row->activity_max_heartrate)) $item['max_heartrate'] = floatval($row->activity_max_heartrate);
  if (isset($row->activity_avg_cadence)) $item['avg_cadence'] = floatval($row->activity_avg_cadence);
  if (isset($row->activity_max_cadence)) $item['max_cadence'] = floatval($row->activity_max_cadence);
  if (isset($row->activity_avg_power)) $item['avg_power'] = floatval($row->activity_avg_power);
  if (isset($row->activity_max_power)) $item['max_power'] = floatval($row->activity_max_power);

  $timer_stops_count = strlen($row->activity_timer_stops) / 4;
  if ($timer_stops_count > 0) {
    $timer_stops = unpack('V' . $timer_stops_count, $row->activity_timer_stops);
    $count = count($timer_stops) + 1;
    $timer_stops_item = array();
    for($i = 1; $i < $count; $i += 2) {
      $from = clone $activity_localtime;
      $from->add(new DateInterval('PT' . $timer_stops[$i] . 'S'));
      $from = $from->format('Y-m-d') . 'T' . $from->format('H:i:s') . $activity_iso_timezone;
      $to = clone $activity_localtime;
      $to->add(new DateInterval('PT' . $timer_stops[$i + 1] . 'S'));
      $to = $to->format('Y-m-d') . 'T' . $to->format('H:i:s') . $activity_iso_timezone;
      $timer_stops_item[] = array($from, $to);
    }
    $item['timer_stops'] = $timer_stops_item;
  }
  
  $item['laps'] = array();
  foreach ($node->openfit_info['laps'] as $lap) {
    $lap_localtime = new DateTime($lap->lap_start, $utc_timezone);
    OpenFitMeasurementDateTime::offsetDate($lap_localtime, $activity_timezone);
    $lap_start_time = $lap_localtime->format('Y-m-d') . 'T' . $lap_localtime->format('H:i:s') . $activity_iso_timezone;
    
    $lap_item = array(
      'number' => intval($lap->lap_number),
      'type' => strtolower($lap->lap_type),
      'start_time' => $lap_start_time,
      'distance' => floatval($lap->lap_distance),
      'duration' => floatval($lap->lap_duration),
      'clock_duration' => floatval($lap->lap_clock_duration),
      'calories' => floatval($lap->lap_calories),
    );
    if (isset($lap->lap_elevation_gain)) $lap_item['elevation_gain'] = floatval($lap->lap_elevation_gain);
    if (isset($lap->lap_elevation_loss)) $lap_item['elevation_loss'] = floatval($lap->lap_elevation_loss);
    if (isset($lap->speed)) $lap_item['avg_speed'] = round($lap->speed, 3);
    if (isset($lap->lap_max_speed)) $lap_item['max_speed'] = round($lap->lap_max_speed, 3);
    if (isset($lap->lap_avg_heartrate)) $lap_item['avg_heartrate'] = floatval($lap->lap_avg_heartrate);
    if (isset($lap->lap_max_heartrate)) $lap_item['max_heartrate'] = floatval($lap->lap_max_heartrate);
    if (isset($lap->lap_avg_cadence)) $lap_item['avg_cadence'] = floatval($lap->lap_avg_cadence);
    if (isset($lap->lap_max_cadence)) $lap_item['max_cadence'] = floatval($lap->lap_max_cadence);
    if (isset($lap->lap_avg_power)) $lap_item['avg_power'] = floatval($lap->lap_avg_power);
    if (isset($lap->lap_max_power)) $lap_item['max_power'] = floatval($lap->lap_max_power);
    if (isset($lap->lap_pool_lengths)) $lap_item['pool_lengths'] = $lap->lap_pool_lengths;
    $item['laps'][] = $lap_item;
  }
  
  $reader = new ActivityDataTrackReader($node->openfit_info['activity']->activity_id, 'full');
  $data = $reader->readTracks();
  foreach($data as $type => $track) {
    $track_data = array();
    switch ($type) {
      case ActivityDataTrackAccess::LOCATION: {
        $time = $track['header']['offset'];
        $data = $track['data'];
        $count = count($data);
        for($i = 0; $i < $count; $i += 2) {
           $track_data[] = $data[$i] + $time;
           $track_data[] = $data[$i + 1] = array(round($data[$i + 1][1], 8), round($data[$i + 1][2], 8));
        }
        break;
      }
      default: {
        $time = $track['header']['offset'];
        $data = $track['data'];
        $count = count($data);
        for($i = 0; $i < $count; $i += 2) {
           $track_data[] = $data[$i] + $time;
           $track_data[] = round($data[$i + 1], 1);
        }
        break;
      }
    }
    if (count($track_data)) $item[$type] = $track_data;
  }
  return $item;
}

/**
 * Query to return the list of activities.
 *
 * @param $page
 *   Page number of results to return. Default to 0.
 * @param $page_size
 *   Maximum number of activities to return per query. Default to 25.
 * @param $no_earlier_than
 *   Limit returned activities to those on or after date. Default to null.
 * @param $no_later_than
 *   'Limit returned activities to those on or before date. Default to null.
 * @param $modified_after
 *   Limit returned activities to those created or modified after date. Default to null.
 * @param $modified_before
 *   'Limit returned activities to those created or modified on or before date. Default to null.
 */
function openfit_api_resource_activity_index($page = 0, $page_size = 25, $no_earlier_than = null, $no_later_than = null, $modified_after = null, $modified_before = null) {
  $page_size = max($page_size, 1);
  $range_start = $page_size * $page;
  $from_date = openfit_api_resource_activity_parse_date($no_earlier_than);
  $to_date = openfit_api_resource_activity_parse_date($no_later_than);
  $mod_from_date = openfit_api_resource_activity_parse_date($modified_after);
  $mod_to_date = openfit_api_resource_activity_parse_date($modified_before);
  global $user;
  if ($user->uid != 0) $uid = $user->uid;
  
  $total_items = 0;
  $items = array();
  $deleted = null;
  
  if (user_access('access content')) {
    $utc_timezone = new DateTimeZone('UTC');

    // Total item count
    $query = 'SELECT COUNT(*) FROM {' . OpenFitActivity::TABLE_ACTIVITY . '} a';
    
    $args = array();
    if (isset($uid)) {
      // If a user is specified, return only those user's workouts.
      $query .= ' WHERE a.uid = :uid';
      $args[':uid'] = $uid;
    } else {
      // Guest access returns only public workouts.
      $query .= ' WHERE a.activity_privacy = :activity_privacy';
      $args[':activity_privacy'] = OpenFitActivity::STATUS_PUBLIC;
    }
    $query .= ' AND a.nid IS NOT NULL';
    if (isset($from_date)) {
      $query .= ' AND a.activity_start >= :from';
      $args[':from'] = $from_date->format('Y-m-d H:i:s');
    }
    if (isset($to_date)) {
      $query .= ' AND a.activity_start <= :to';
      $args[':to'] = $to_date->format('Y-m-d H:i:s');
    }
    if (isset($modified_after)) {
      $query .= ' AND FROM_UNIXTIME(a.changed) > :after';
      $args[':after'] = $mod_from_date->format('Y-m-d H:i:s');
    }
    if (isset($modified_before)) {
      $query .= ' AND FROM_UNIXTIME(a.changed) <= :before';
      $args[':before'] = $mod_to_date->format('Y-m-d H:i:s');
    }
    $total_items = db_query($query, $args)->fetchField();

    // Items for selected page
    $query = db_select(OpenFitActivity::TABLE_ACTIVITY, 'a');
    $query->range($range_start, $page_size);
    $query->innerJoin(OpenFitActivity::TABLE_ACTIVITY_CATEGORY, 'c', 'a.activity_category_id = c.category_id');
    $query
      ->fields('a', array('nid', 'uid', 'changed', 'created', 'activity_name', 'activity_start', 'activity_timezone', 'activity_distance', 'activity_duration'))
      ->fields('c', array('category_id', 'category_name', 'category_noun', 'category_image_url'));
    // If querying for a specific user after a modified date, sort by oldest modified first
    if (isset($uid) && isset($mod_from_date)) {
      $query->orderBy('a.changed');
    } else {
      // Otherwise sort by most recent start first
      $query->orderBy('activity_start', 'DESC');
    }
    $query->orderBy('nid', 'DESC');
    $query->isNotNull('a.nid');
    if (isset($uid)) {
      // If a user is specified, return only those user's workouts.
      $query->condition('a.uid', $uid);
    } else {
      // Guest access returns only public workouts.
      $query->condition('a.activity_privacy', OpenFitActivity::STATUS_PUBLIC);
    }
    if (isset($from_date)) {
      $query->condition('a.activity_start', $from_date->format('Y-m-d H:i:s'), '>=');
    }
    if (isset($to_date)) {
      $query->condition('a.activity_start', $to_date->format('Y-m-d H:i:s'), '<=');
    }
    if (isset($modified_after)) {
      $query->condition('a.changed', $mod_from_date->getTimestamp(), '>');
    }
    if (isset($modified_before)) {
      $query->condition('a.changed', $mod_to_date->getTimestamp(), '<=');
    }
    $results = $query->execute();
    while ($row = $results->fetchAssoc()) {
      $row = (object)$row;
      $nid = $row->nid;
      $activity_localtime = new DateTime($row->activity_start, new DateTimeZone('UTC'));
      $activity_timezone = $row->activity_timezone;
      OpenFitMeasurementDateTime::offsetDate($activity_localtime, $activity_timezone);
      $start_time = $activity_localtime->format('Y-m-d') . 'T' . $activity_localtime->format('H:i:s') . OpenFitMeasurementDateTime::getISOTimezone($activity_timezone);
      if (isset($uid) && isset($mod_from_date)) {
        $changed_at = new DateTime();
        $changed_at->setTimestamp($row->changed);
        $changed_at->setTimezone($utc_timezone);
        $changed_at = $changed_at->format('Y-m-d') . 'T' . $changed_at->format('H:i:s') . 'Z';
        $created_at = new DateTime();
        $created_at->setTimestamp($row->created);
        $created_at->setTimezone($utc_timezone);
        $created_at = $created_at->format('Y-m-d') . 'T' . $created_at->format('H:i:s') . 'Z';
      }
      $uri = openfit_api_resource_activity_resource_uri(array('activity', $nid));
      $item = array(
        'start_time' => $start_time,
        'total_distance' => $row->activity_distance,
        'duration' => $row->activity_duration,
        'type' => $row->category_name,
        'name' => $row->activity_name,
        'user_id' => $row->uid,
        'uri' => $uri,
      );
      if (isset($uid) && isset($mod_from_date)) {
        $item['changed'] = $changed_at;
        $item['created'] = $created_at;
      }
      $items[] = $item;
    }
    
    // If querying for a specific user after a modified date, include deleted
    if ($page == 0 && isset($uid) && isset($mod_from_date)) {
      $query = db_select(OpenFitActivity::TABLE_ACTIVITY, 'a');
      $query->innerJoin(OpenFitActivity::TABLE_ACTIVITY_DELETED, 'd', 'a.activity_id = d.activity_id');
      $query->fields('a', array('changed'))
               ->fields('d', array('nid'));
      $query->condition('a.uid', $uid);
    
      if (isset($modified_after)) {
        $query->condition('a.changed', $mod_from_date->getTimestamp(), '>');
      }
      if (isset($modified_before)) {
        $query->condition('a.changed', $mod_to_date->getTimestamp(), '<=');
      }
      $results = $query->execute();
      $deleted = array();
      while ($row = $results->fetchAssoc()) {
        $row = (object)$row;
        $nid = $row->nid;
        $uri = openfit_api_resource_activity_resource_uri(array('activity', $nid));
        $deleted[] = $uri;
      }
    } 
  }
  $ret = array('size' => $total_items, 'items' => $items);
  if (isset($deleted)) $ret['deleted'] = $deleted;
  if ($range_start > 0) {
    $ret['previous'] = openfit_api_resource_activity_get_index_page_url($page - 1, $page_size, $no_earlier_than, $no_later_than, $modified_after, $modified_before);
  }
  if ($range_start + $page_size < $total_items) {
    $ret['next'] = openfit_api_resource_activity_get_index_page_url($page + 1, $page_size, $no_earlier_than, $no_later_than, $modified_after, $modified_before);
  }
  return $ret;
}

function openfit_api_resource_activity_access($mode = 'create', $nid = null) {
  $node = null;
  switch ($mode) {
    case 'create':
      $node = OpenFitActivity::NODE_TYPE_ACTIVITY;
      break;
    default:
      if (isset($nid)) {
        $node = node_load($nid);
      }
      break;
  }
  if ((!isset($node) || !isset($node->nid) || $node->nid == 0) && $mode == 'delete') return TRUE;
  return (node_access($mode, $node) || user_access('administer content types'));
}

function openfit_api_resource_get_fields() {
  return array(
    'copy' => array(
      'category' => 'type',
      'name' => 'name',
      'notes' => 'notes',
      'location_name' => 'location_name',
    ),
    'ufloat' => array(
      'distance' => 'total_distance',
      'duration' => 'duration',
      'clock_duration' => 'clock_duration',
      'calories' => 'calories',
      'elevation_gain' => 'elevation_gain',
      'elevation_loss' => 'elevation_loss',
    ),
    'avgmax' => array(
      'heartrate' => 'heartrate',
      'cadence' => 'cadence',
      'power' => 'power',
    ),
    'lap_ufloat' => array(
      'distance' => 'distance',
      'duration' => 'duration',
      'clock_duration' => 'clock_duration',
      'calories' => 'calories',
      'elevation_gain' => 'elevation_gain',
      'elevation_loss' => 'elevation_loss',
    ),
    'lap_avgmax' => array(
      'heartrate' => 'heartrate',
      'cadence' => 'cadence',
      'power' => 'power',
    ),
  );
}

function openfit_api_resource_parse_activity_data($data) {
  $fields = openfit_api_resource_get_fields();
  $avgmax_subfields = array('avg','max');
  
  $activity = array();
  $activity_start_time = null;
  if (isset($data['start_time'])) {
    try {
      $activity_start_time = new DateTime($data['start_time']);
      $activity['timezone'] = $activity_start_time->getOffset() / 3600;
      $activity_start_time->setTimezone(new DateTimeZone('UTC'));
      $activity['start_time'] = $activity_start_time->format('Y-m-d H:i:s');
    } catch (Exception $e) {
      openfit_api_resource_process_error(406, 'Invalid activity start time: ' . $data['start_time']);
    }
  }
  if (!isset($activity_start_time)) openfit_api_resource_process_error(406, 'Missing activity start time');
  if (isset($data['privacy'])) {
    $statuses = openfit_api_resource_activity_statuses();
    foreach ($statuses as $id => $val) {
      if ($data['privacy'] == $val) $activity['privacy'] = $id;
    }
  }
  if (isset($data['max_speed'])) $activity['speed']['max'] = max(0,floatval($data['max_speed']));
  foreach ($fields['copy'] as $to => $from) {
    if (isset($data[$from])) $activity[$to] = $data[$from];
  }
  foreach ($fields['ufloat'] as $to => $from) {
    if (isset($data[$from])) $activity[$to] = max(0,floatval($data[$from]));
  }
  foreach ($fields['avgmax'] as $to => $from) {
    foreach ($avgmax_subfields as $subfield) {
      $from_field = $subfield . '_' . $from;
      if (isset($data[$from_field])) $activity[$to][$subfield] = max(0,floatval($data[$from_field]));
    }
  }
  $track_version = 0;
  $time_data_format = ActivityDataTrackAccess::getTimeDataFormat($track_version);
  $data_tracks = array();
  $data_track_types = ActivityDataTrackAccess::getAllDataTrackTypes();
  foreach ($data_track_types as $track_type)
  {
    if (!isset($data[$track_type])) continue;
    $track_data = $data[$track_type];
    if (!is_array($track_data)) continue;
    $count = count($track_data);
    if ($count < 2) continue;
    $data_format = ActivityDataTrackAccess::getDataFormat($track_type);
    $track = pack('S', $track_version);
    $data_tracks[$track_type] = array();

    $elapsed = 0;
    $pt_value = null;
    $pt_prev_elapsed = null;
    $pt_prev_negative = null;

    for ($i = 0; $i < $count; $i += 2) {
      $elapsed = $track_data[$i];
      $pt_value = $track_data[$i+1];
      // Skip any points before elapsed = 0
      if ($elapsed < 0) {
        $pt_prev_elapsed = $elapsed;
        $pt_prev_negative = $pt_value;
        continue;
      }
      // If there were points before elapsed = 0, interpolate a straight line to the value at elapsed = 0
      if (isset($pt_prev_negative) && $elapsed != 0) {
        if ($track_type == ActivityDataTrackAccess::LOCATION) {
          $pt_value[0] = $pt_prev_negative[0] + ($pt_value[0] - $pt_prev_negative[0]) * -$pt_prev_elapsed / ($elapsed - $pt_prev_elapsed);
          $pt_value[1] = $pt_prev_negative[1] + ($pt_value[1] - $pt_prev_negative[1]) * -$pt_prev_elapsed / ($elapsed - $pt_prev_elapsed);
        } else {
          $pt_value = $pt_prev_negative + ($pt_value - $pt_prev_negative) * -$pt_prev_elapsed / ($elapsed - $pt_prev_elapsed);
        }
        $elapsed = 0;
        $pt_prev_negative = null;
      }
      if ($track_type == ActivityDataTrackAccess::LOCATION) {
        $track .= pack($time_data_format, intval($elapsed)) . pack($data_format, $pt_value[0], $pt_value[1]);
      } else {
        $track .= pack($time_data_format, intval($elapsed)) . pack($data_format, $pt_value);
      }
    }
    
    $data_tracks[$track_type] = $track;
  }
  $activity['data_tracks'] = $data_tracks;
  
  if (isset($data['laps']) && is_array($data['laps'])) {
    $laps = array();
    foreach ($data['laps'] as $lap_data) {
      $lap = array();
      try {
        $lap['start_time'] = new DateTime($lap_data['start_time']);
        $lap['start_time']->setTimezone(new DateTimeZone('UTC'));
        $lap['start_time'] = $lap['start_time']->format('Y-m-d H:i:s');
      } catch (Exception $e) {
        openfit_api_resource_process_error(406, 'Invalid lap start time: ' . $lap_data['start_time']);
      }
      if (isset($lap_data['type'])) $lap['type'] = $lap_data['type'];
      if (isset($lap_data['max_speed'])) $lap['speed']['max'] = max(0,floatval($lap_data['max_speed']));
      foreach ($fields['lap_ufloat'] as $to => $from) {
        if (isset($lap_data[$from])) $lap[$to] = max(0,floatval($lap_data[$from]));
      }
      foreach ($fields['lap_avgmax'] as $to => $from) {
        foreach ($avgmax_subfields as $subfield) {
          $from_field = $subfield . '_' . $from;
          if (isset($lap_data[$from_field])) $lap[$to][$subfield] = max(0,floatval($lap_data[$from_field]));
        }
      }
      $laps[] = $lap;
    }
    $activity['laps'] = $laps;
  }

  if (isset($data['timer_stops']) && is_array($data['timer_stops'])) {
    $timer_stops = array();
    foreach ($data['timer_stops'] as $timer_stop_data) {
      $from = null;
      $to = null;
      try {
        $from = new DateTime($timer_stop_data[0]);
        $from->setTimezone(new DateTimeZone('UTC'));
      } catch (Exception $e) {
        openfit_api_resource_process_error(406, 'Invalid timer pause from time: ' . $timer_stop_data[0]);
      }
      try {
        $to = new DateTime($timer_stop_data[1]);
        $to->setTimezone(new DateTimeZone('UTC'));
      } catch (Exception $e) {
        openfit_api_resource_process_error(406, 'Invalid timer pause to time: ' . $timer_stop_data[1]);
      }
      if (isset($from) && isset($to)) $timer_stops[] = array($from, $to);      
    }
    $elapsed_timer_stops = array();
    foreach ($timer_stops as $timer_stop) {
      $from = $activity_start_time->diff($timer_stop[0]);
      $from = (($from->days * 24 + $from->h) * 60 + $from->i) * 60 + $from->s;
      $to = $activity_start_time->diff($timer_stop[1]);
      $to = (($to->days * 24 + $to->h) * 60 + $to->i) * 60 + $to->s;
      $elapsed_timer_stops[] = array('from' => $from, 'to' => $to);
    }
    $activity['timer_stops'] = $elapsed_timer_stops;
  }
  
  return $activity;
}

function openfit_api_resource_activity_create($data) {
  $activity = openfit_api_resource_parse_activity_data($data);
  
  $transaction = db_transaction();

  try {
    $nodes = OpenFitActivity::createActivityNodes(array($activity));
    $uris = array();
    foreach ($nodes as $node) {
      OpenFitActivity::insertActivityRecords($node);
      $uris[] = openfit_api_resource_activity_resource_uri(array('activity', $node->nid));
    }
    return array('uris' => $uris);
  } catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('sporttracks', $e);
    throw $e;
  }
}

function openfit_api_resource_activity_update($nid, $data) {
  $new_activity = openfit_api_resource_parse_activity_data($data);
  
  OpenFitActivity::replaceData($nid, $new_activity);
  return array('uri' => openfit_api_resource_activity_resource_uri(array('activity', $nid)));
}

function openfit_api_resource_activity_delete($nid) {
  $to_delete = array();
  $to_delete[] = $nid;
  node_delete_multiple($to_delete);
  return array('status' => 'ok');
}

function openfit_api_resource_process_error($code, $message) {
  $data = array('error' => array('code' => $code, 'message' => $message));
  services_error($message, $code, $data);
}

function openfit_api_resource_activity_parse_date($date) {
  if (!isset($date)) return null;
  try {
    $dt = new DateTime($date, new DateTimeZone('UTC'));
    $dt->setTimezone(new DateTimeZone(variable_get('date_default_timezone', 'America/New_York')));
    return $dt;
  } catch (Exception $e) {
    return null;
  }
}

function openfit_api_resource_activity_get_index_page_url($page, $page_size, $no_earlier_than, $no_later_than, $modified_after, $modified_before) {
  $url_params = array('page' => $page);
  if ($page_size != 25) $url_params['pageSize'] = $page_size;
  if (isset($no_earlier_than)) $url_params['noEarlierThan'] = $no_earlier_than;
  if (isset($no_later_than)) $url_params['noLaterThan'] = $no_later_than;
  if (isset($modified_after)) $url_params['modifiedAfter'] = $modified_after;
  if (isset($modified_before)) $url_params['modifiedBefore'] = $modified_before;
  return openfit_api_resource_activity_resource_uri(array('activity'), $url_params);
}

function openfit_api_resource_activity_resource_uri($path, $params = null) {
  $request_type = '';
  $request = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  $dot_pos = strrpos($request,'.');
  if (!($dot_pos === FALSE)) $request_type = substr($request,$dot_pos);
  $uri = services_resource_uri($path) . $request_type;
  if (isset($params) && count($params) > 0) $uri.= '?' . http_build_query($params, '', '&');
  return $uri;
}