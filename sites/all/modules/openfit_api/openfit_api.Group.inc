<?php 

class OpenFitGroup {

  const TABLE_GROUP_MEMBER = 'openfit_group_member';
  const GROUP_TYPE_USER = 'user';

  public static function getSchema(&$schema) {  
    $schema[self::TABLE_GROUP_MEMBER] = array(
      'description' => 'OpenFit group members.',
      'fields' => array(
        'group_type' => array(
          'description' => 'Group type.',
          'type' => 'varchar',
          'length' => 10,
          'not null' => TRUE,
        ),
        'group_id' => array(
          'description' => 'Group id.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'uid' => array(
          'description' => 'User id.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'group_joined' => array(
          'description' => 'The Unix timestamp when the user joined the group.',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'group_role' => array(
          'description' => 'The users role in the group.',
          'type' => 'varchar',
          'length' => 10,
        ),
      ),
      'primary key' => array('group_type', 'group_id', 'uid'),
    );
  }
  
  /**
   * Return true if $friend_uid is connected to $uid in any way.
   */
  public static function isFriend($uid, $friend_uid) {
    $is_friend = db_query('SELECT 1 FROM {' . OpenFitGroup::TABLE_GROUP_MEMBER . '} WHERE group_type=:type AND group_id=:uid AND uid=:fuid', 
      array(':type' => OpenFitGroup::GROUP_TYPE_USER, ':uid' => $uid, ':fuid' => $friend_uid))->fetchField();
    return $is_friend > 0;
  }
  
  /**
   * Return a list of the user's friends.
   *
   * @param $sort_by_header
   *   If not null, use the header column information and $_GET['q'] and $_GET['order'] to sort the members. Otherwise sort by full name.
   * @param $page_size
   *   If not null, return $page_size number of rows and get the current page from $_GET['page']. Otherwise return all members.
   * @return An associative array of rows indexed by uid.
   */
  public static function getFriends($sort_by_header = null, $page_size = null) {
    global $user;
    $me = $user->uid;
    return self::getMembers(OpenFitGroup::GROUP_TYPE_USER, $me, $page_size);
  }
  
  /**
   * Remove a friend.
   *
   * @param $uid
   *   Friend's user id.
   */
  public static function removeFriend($uid) {
    global $user;
    
    $transaction = db_transaction();
    try {
      // Delete friend from me
      db_delete(OpenFitGroup::TABLE_GROUP_MEMBER)
        ->condition('group_type', OpenFitGroup::GROUP_TYPE_USER)
        ->condition('group_id', $user->uid)
        ->condition('uid', $uid)
        ->execute();
      // Delete me from friend
      db_delete(OpenFitGroup::TABLE_GROUP_MEMBER)
        ->condition('group_type', OpenFitGroup::GROUP_TYPE_USER)
        ->condition('group_id', $uid)
        ->condition('uid', $user->uid)
        ->execute();
    } catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('sporttracks', $e);
      throw $e;
    }
  }
  
  /**
   * Get the total count of members of a group.
   * @param $group_type
   *   The group type.
   * @param $group_id
   *   The group ID.
   */
  public static function getMembersCount($group_type, $group_id) {
    $query = 'SELECT COUNT(*) FROM {' . OpenFitGroup::TABLE_GROUP_MEMBER . '} WHERE group_type = :type AND group_id  = :id';
    $args = array(':type' => $group_type, ':id' => $group_id);
    return db_query($query, $args)->fetchField();
  }
  
  /**
   * Return a list of the members of a group.
   *
   * @param $group_type
   *   The group type.
   * @param $group_id
   *   The group ID.
   * @param $sort_by_header
   *   If not null, use the header column information and $_GET['q'] and $_GET['order'] to sort the members. Otherwise sort by full name.
   * @param $page_size
   *   If not null, return $page_size number of rows and get the current page from $_GET['page']. Otherwise return all members.
   * @return An associative array of rows indexed by uid.
   */
  public static function getMembers($group_type, $group_id, $sort_by_header = null, $page_size = null) {
    $fullname_query = db_select(OpenFitUserSetting::TABLE_SETTING, 's');
    $fullname_query->fields('s', array('uid', 'value'));
    $fullname_query->condition('name', OpenFitUserSetting::TYPE_FULLNAME);
    
    $query = db_select(OpenFitGroup::TABLE_GROUP_MEMBER, 'm');
    if (is_array($sort_by_header)) $query = $query->extend('TableSort');
    if (is_numeric($page_size)) {
      $query = $query->extend('PagerDefault');
      $query->limit($page_size);
    }
    $query->innerJoin('users', 'u', 'm.uid = u.uid');
    $query->addJoin('LEFT OUTER', $fullname_query, 's', 'm.uid = s.uid');
    $query->fields('u', array('uid', 'picture'));
    $query->addExpression('IFNULL( CAST(s.value AS CHAR CHARACTER SET utf8), u.name)', 'name');
    
    if (is_array($sort_by_header)) {
      $query->orderByHeader($sort_by_header);
    } else {
      $query->orderBy('name')->orderBy('group_joined');
    }
    $query->condition('m.group_type', $group_type);
    $query->condition('m.group_id', $group_id);
    $results = $query->execute();
    $members = array();
    if ($results) {
      while ($row = $results->fetchAssoc()) $members[$row['uid']] = (object)$row;
    }
    return $members;
  }
}