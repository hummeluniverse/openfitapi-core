<?php 

class OpenFitMeasurement{

  const COL_ID = 0;
  const COL_TYPE = 1;
  const COL_SINGLE = 2;
  const COL_PLURAL = 3;
  const COL_SYMBOL = 4;
  const COL_DECIMALS = 5;
  const COL_FACTOR = 6;
  const COL_OFFSET = 7;
  
  /**
   * Measurement units moved out of database and into code for performance reasons.
   * Array columns:
   *  - id: Machine readable unique id.
   *  - type: Type of measurement.
   *  - single: Localizable singular name.
   *  - plural: Localizable plural name.
   *  - symbol: Localizable shorthand symbol (abbreviation).
   *  - decimals: Default number of digits after the decimal.
   *  - conversion_factor: Factor to multiply the value by to convert to SI units.
   *  - conversion_offset: Factor to add to the value to convert to SI units.
   */
  private static $unitInfo = array(
    'centimeter' => array('centimeter','length','centimeter','centimeters','cm',0,0.1,0),
    'meter' => array('meter','length','meter','meters','m',0,1,0),
    'kilometer' => array('kilometer','length','kilometer','kilometers','km',1,1000,0),
    'inch' => array('inch','length','inch','inches','in',0,0.0254,0),
    'foot' => array('foot','length','foot','feet','ft',0,0.3048,0),
    'yard' => array('yard','length','yard','yards','yd',0,0.9144,0),
    'mile' => array('mile','length','mile','miles','mi',2,1609.344,0),
    'nauticalmile' => array('nauticalmile', 'length', 'nautical mile', 'nautical miles', 'NM', 0, 1852, 0),
    
    'meter-per-second' => array('meter-per-second','speed','meter per second','meters per second','m/s',0,1,0),
    'kilometer-per-hour' => array('kilometer-per-hour','speed','kilometer per hour','kilometers per hour','km/h',1,0.2777777778,0),
    'mile-per-hour' => array('mile-per-hour','speed','mile per hour','miles per hour','mph',1,0.44704,0),
    
    'gram' => array('gram','weight','gram','grams','g',1,0.001,0),
    'kilogram' => array('kilogram','weight','kilogram','kilograms','kg',1,1,0),
    'ounce' => array('ounce','weight','ounce','ounces','oz',0,0.0283495231,0),
    'pound' => array('pound','weight','pound','pounds','lb',0,0.45359237,0),
  
    'celsius' => array('celsius','temperature','celsius','celsius','&deg;C',1,1,0),
    'fahrenheit' => array('fahrenheit','temperature','fahrenheit','fahrenheit','&deg;F',0,0.55555555555555, 32),
    
    'kilojoule' => array('kilojoule','energy','kilojoule','kilojoules','kJ',0,1,0),
    'calorie' => array('calorie','energy','calorie','calories','cal',0,4.184,0),
    
    'cupcake' => array('cupcake', 'energy', 'cupcake', 'cupcakes', 'cupcakes', 0, 669.44, 0),
    'fortnight' => array('fortnight', 'time', 'fortnight', 'fortnights', 'fortnights', 0, 1468800, 0),
    'furlong' => array('furlong', 'length', 'furlong', 'furlongs', 'furlongs', 0, 201.168, 0),
    'tripmoon' => array('tripmoon', 'length', 'trip to the moon', 'trips to the moon', 'trips to the moon', 0, 385000000, 0),
    'tripearth' => array('tripearth', 'length', 'time around the Earth', 'times around the Earth', 'times around the Earth', 0, 40075020, 0),
    'huskydogs' => array('huskydogs', 'energy', 'day worth of calories consumed by an Alaskan sled dog', 'days worth of calories consumed by an Alaskan sled dog', 'days worth of calories consumed by an Alaskan sled dog', 0, 20920, 0),
    'gallonofgas' => array('gallonofgas', 'energy', 'gallon of gasoline', 'gallons of gasoline', 'gallons of gasoline', 0, 131078.029, 0),
    'dogyear' => array('dogyear', 'time', 'dog year', 'dog years', 'dog years', 0, 220898482, 0),
  );
  
  const SYSTEM_METRIC = 'metric';
  const SYSTEM_US = 'us';
  
  const MEASUREMENT_DISTANCE = 'distance';
  const MEASUREMENT_SPEED = 'speed';
  const MEASUREMENT_ELEVATION = 'elevation';
  const MEASUREMENT_WEIGHT = 'weight';
  const MEASUREMENT_TEMPERATURE = 'temperature';
  const MEASUREMENT_CALORIE = 'calorie';
  
  const DEFAULT_DISTANCE_UNITS = 'kilometer';
  const DEFAULT_SPEED_UNITS = 'kilometer-per-hour';
  const DEFAULT_ELEVATION_UNITS = 'meter';
  const DEFAULT_WEIGHT_UNITS = 'kilogram';
  const DEFAULT_TEMPERATURE_UNITS = 'celsius';
  const DEFAULT_CALORIE_UNITS = 'kilojoule';
  
  const FORMAT_TYPE_NONE = 'none';
  const FORMAT_TYPE_DECIMALS = 'decimals';
  const FORMAT_TYPE_LABEL = 'label';
  const FORMAT_TYPE_LABEL_CAPS = 'caps';
  const FORMAT_TYPE_SYMBOL = 'symbol';
  
  const UNITS_CALORIE = 'calorie';
  
  /**
   * Get information about the measurement systems (eg "US" or "Metric").
   *
   * Returns an associative array indexed by a SYSTEM_* constant that consists of:
   *   - title: A non-localized system title
   *   - units: An associative array indexed by MEASUREMENT_* which indicates the default units.
   */
  public static function getMeasurementSystems() {
    return array(
      self::SYSTEM_METRIC => (object)array(
        'title' => 'Metric',
        'units' => array(
          self::MEASUREMENT_DISTANCE => 'kilometer',
          self::MEASUREMENT_SPEED => 'kilometer-per-hour',
          self::MEASUREMENT_ELEVATION => 'meter',
          self::MEASUREMENT_WEIGHT => 'kilogram',
          self::MEASUREMENT_TEMPERATURE => 'celsius',
          self::MEASUREMENT_CALORIE => 'kilojoule',
        ),
      ),
      self::SYSTEM_US => (object)array(
        'title' => 'US',
        'units' => array(
          self::MEASUREMENT_DISTANCE => 'mile',
          self::MEASUREMENT_SPEED => 'mile-per-hour',
          self::MEASUREMENT_ELEVATION => 'foot',
          self::MEASUREMENT_WEIGHT => 'pound',
          self::MEASUREMENT_TEMPERATURE => 'fahrenheit',
          self::MEASUREMENT_CALORIE => 'calorie',
        ),
      ),
    );
  }
  
  /**
    * Get the measurement units for the system.
    */
  public static function getUnitName($system, $unit) {
    $systems = self::getMeasurementSystems();
    return $systems[$system]->units[$unit];
  }
  
  /**
   * Convert a value from one unit system to another and optionally format the output.
   *
   * @param $value
   *   The value to convert from.
   * @param $units_from
   *   The units to convert from.
   * @param $units_to
   *   The units to convert to.
   * @param $format
   *   Optional parameter to format the output by rounding decimals and adding a unit label (eg "1.00 mi").
   * @param $decimals
   *   Number of decimals to output if $format is something other than FORMAT_TYPE_NONE. If null, use the default unit decimals.
   */
  public static function convert($value, $units_from, $units_to, $format = self::FORMAT_TYPE_NONE, $decimals = null) {
    if (!is_numeric($value)) return null;

    $rows = array();
    if (isset(self::$unitInfo[$units_from])) $rows[$units_from] = self::$unitInfo[$units_from];
    if (isset(self::$unitInfo[$units_to])) $rows[$units_to] = self::$unitInfo[$units_to];
      
    // Check for valid units. If either fail keep processing with raw values
    if (isset($units_from) && strlen($units_from) > 0 && !isset($rows[$units_from])) {
      watchdog('openfit_api','Cannot find unit id "' . $units_from . '" to convert from.', null, WATCHDOG_ERROR);
    }
    if (isset($units_to) && strlen($units_to) > 0 && !isset($rows[$units_to])) {
      watchdog('openfit_api','Cannot find unit id "' . $units_to . '" to convert to.', null, WATCHDOG_ERROR);
    }
    
    // Convert the incoming value to SI units
    if (isset($rows[$units_from])) {
      if ($rows[$units_from][self::COL_FACTOR] != 1 || $rows[$units_from][self::COL_OFFSET] != 0) {
        $value -= $rows[$units_from][self::COL_OFFSET];
        $value *= $rows[$units_from][self::COL_FACTOR];
      }
    }
    
    // Convert from SI units to the outgoing value
    if (isset($rows[$units_to])) {
      if ($rows[$units_to][self::COL_FACTOR] != 1 || $rows[$units_to][self::COL_OFFSET] != 0) {
        $value /= $rows[$units_to][self::COL_FACTOR];
        $value += $rows[$units_to][self::COL_OFFSET];
      }
    }
      
    // Round to the appropriate decimals
    if (!isset($decimals) && isset($rows[$units_to])) {
      $decimals = $rows[$units_to][self::COL_DECIMALS];
    }
    if (isset($decimals) && $format != self::FORMAT_TYPE_NONE) {
      $pattern =  '#,##0';
      if ($decimals > 0) {
        $pattern .= '.' . str_repeat('0', $decimals);
      }
      $fmt = new NumberFormatter(OpenFitUserSetting::getCurrentUserLocale(), NumberFormatter::DECIMAL);
      $fmt->setPattern($pattern);
      $value = $fmt->format($value);
    }
    
    // Add the units label
    if (isset($rows[$units_to])) {
      $label = null;
      switch ($format) {
        case self::FORMAT_TYPE_LABEL:
        case self::FORMAT_TYPE_LABEL_CAPS:
          if ($value == 1) {
            $label = $rows[$units_to][self::COL_SINGLE];
          } else {
            $label = $rows[$units_to][self::COL_PLURAL];
          }
          if ($format == self::FORMAT_TYPE_LABEL_CAPS) $label = ucfirst($label);
          break;
        case self::FORMAT_TYPE_SYMBOL:
          $label = $rows[$units_to][self::COL_SYMBOL];
          break;
      }
      // If $label is set $value is changed from a numeric to a string
      if (isset($label)) $value .= ' ' . $label;
    }
    
    return $value;
  }
  
    public static function getConversionInfo($units) {
      if (!isset(self::$unitInfo[$units])) {
        watchdog('openfit_api','Cannot find unit id "' . $units . '" to convert from.', null, WATCHDOG_ERROR);
        return null;
      }
      $info = self::$unitInfo[$units];
      return (object)array(
        'unit_id' => $info[self::COL_ID],
        'unit_type' => $info[self::COL_TYPE],
        'unit_single' => $info[self::COL_SINGLE],
        'unit_plural' => $info[self::COL_PLURAL],
        'unit_symbol' => $info[self::COL_SYMBOL],
        'unit_decimals' => $info[self::COL_DECIMALS],
        'conversion_factor' => $info[self::COL_FACTOR],
        'conversion_offset' => $info[self::COL_OFFSET],
      );
    }
    
  /**
   * Get the label for the specified units in the specified format.
   *
   * @param $value
   *   The value used to determine if a single or plural label is returned.
   * @param $units
   *   The unit id.
   * @param $format
   *   The format type
   * @return
   *   The formatted label type for the units or blank if not found.
   */
  public static function getLabel($value, $units, $format) {
    if ($format == self::FORMAT_TYPE_NONE || $format == self::FORMAT_TYPE_DECIMALS) return '';
    
    if (!isset(self::$unitInfo[$units])) {
      watchdog('openfit_api','Cannot find label for unit id "' . $units . '".', null, WATCHDOG_ERROR);
      return '';
    }
    $info = self::$unitInfo[$units];
    
    switch ($format) {
      case self::FORMAT_TYPE_LABEL:
      case self::FORMAT_TYPE_LABEL_CAPS:
        if ($value == 1) {
          $label = $info[self::COL_SINGLE];
        } else {
          $label = $info[self::COL_PLURAL];
        }
        if ($format == self::FORMAT_TYPE_LABEL_CAPS) $label = ucfirst($label);
        return t($label);
      case self::FORMAT_TYPE_SYMBOL:
        return t($info[self::COL_SYMBOL]);
    }
    return '';
  }
  
  public static function getCalorieTitle($calorie_units) {
    if ($calorie_units != self::UNITS_CALORIE) {
      $symbol = self::getLabel(1, $calorie_units, self::FORMAT_TYPE_SYMBOL);
      return t('Energy (@units)', array('@units' => $symbol));
    } else {
      return t('Calories');
    }
  }
  
  /**
   * Return an associative array of an attempt to parse the specified text into a measurement value.
   *
   * Array elements:
   *   - valid: A boolean indicating whether the parsing was valid
   *   - value: The value in the unit type specified.
   *
   * @param $text
   *   The text to parse.
   * @param $value_units
   *   The unit type to return the value in. If null the value is returned as-is without conversion.
   * @param $default_units
   *   The default unit type if the user does not explicitly specify it. Modified to return the value the user enters.
   */
  public static function parse($text, $value_units = null, &$default_units = null) {
    $values = array('valid' => TRUE, 'value' => 0);
    $text = trim($text);
    // Fix for PHP bug in NumberFormatter::parse() when using locales which have space for thousands separator (French).
    $text = str_replace(' ','',$text); 
    $len = strlen($text);
    
    // Blank will always return zero.
    if ($len == 0) return $values;
    
    $revtext = strrev($text);
    $matches = array();
    $num_match = preg_match('/[0-9,.]/', $revtext, $matches, PREG_OFFSET_CAPTURE);
    
    // If there are no numeric digits return zero.
    if ($num_match == 0 || count($matches) == 0) return $values;
    
    $last_digit = $len - 1 - $matches[0][1];
    $meters = 0;
    if ($last_digit != $len - 1) {
      $unit_text = substr($text, $last_digit + 1);
      $text = substr($text, 0, $last_digit + 1);
      if (isset($value_units) && isset($default_units)) {
        if (isset(self::$unitInfo[$value_units])) {
          $unit_type = self::$unitInfo[$value_units][self::COL_TYPE];
          $unit_id = self::parseUnits($unit_text, $unit_type);
          if (isset($unit_id)) $default_units = $unit_id;
        }
      }
    }
    $fmt = new NumberFormatter(OpenFitUserSetting::getCurrentUserLocale(), NumberFormatter::DECIMAL);
    $value = $fmt->parse($text);
    if ($value === false) {
      $values['valid'] = FALSE;
      return $values;
    }
    if (isset($value_units) && isset($default_units)) $value = self::convert($value, $default_units, $value_units);
    $values['value'] = $value;
    return $values;
  }

  /**
   * Return the unit id of the text or null if not found.
   *
   * Do a case-insensitive lookup of the text against unit measurements for the specified type in this order:
   *   - localized symbol text (eg "mi")
   *   - localized single text (eg "mile")
   *   - localized plural text (eg "miles")
   *
   * @param $text
   *   The text to look up.
   * @param $unit_type
   *   A MEASUREMENT_* constant (eg MEASUREMENT_DISTANCE, MEASUREMENT_WEIGHT, etc).
   * @return
   *   If found, the unit id of the specified text, otherwise null.
   */   
  private static function parseUnits($text, $unit_type) {
    $text = strtolower(trim($text));
    if ($unit_type == self::MEASUREMENT_DISTANCE || $unit_type == self::MEASUREMENT_ELEVATION) $unit_type = 'length';
        
    foreach (self::$unitInfo as $unit_id => $info) {
      if (strcasecmp(t($info[self::COL_SYMBOL]), $text) == 0) return $unit_id;
    }
    foreach ($rows as $unit_id => $row) {
      if (strcasecmp(t($info[self::COL_SINGLE]), $text) == 0) return $unit_id;
    }
    foreach ($rows as $unit_id => $row) {
      if (strcasecmp(t($info[self::COL_PLURAL]), $text) == 0) return $unit_id;
    }
    return null;
  }  
}