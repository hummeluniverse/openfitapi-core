<?php

/**
 * Returns HTML for a small query pager.
 *
 * Menu callbacks that display paged query results should call theme('small_pager') to
 * retrieve a pager control so that users can view other results. Format a list
 * of nearby pages with additional query results.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An optional integer to distinguish between multiple pagers on one page.
 *   - options: Associative array of display options
 *     - hide_empty: If TRUE, hide the entire pager when not needed (default to TRUE)
 *   - attributes: Class attributes for the entire pager.
 *
 * @ingroup themeable
 */
function theme_small_pager($variables = array()) {
  global $pager_page_array, $pager_total, $pager_total_items, $pager_limits;
  $variables += array('attributes' => array(), 'element' => 0,  'options' => array());
  $variables['options'] += array(
    'hide_empty' => TRUE,
    'button_classes' => array(),    
    'button_location' => 'after', 
    'ajax' => FALSE, 'ajax_loaded' => FALSE);
  
  $element = $variables['element'];
  $options = $variables['options'];
  $attributes = $variables['attributes'];
  
  $ajax = isset($options) && isset($options['ajax']) && $options['ajax'];
  $ajax_loaded = isset($options) && isset($options['ajax_loaded']) && $options['ajax_loaded'];
  
  if (!$ajax || $ajax_loaded) {
    $total_items = $pager_total_items[$element];
    $items_per_page = $pager_limits[$element];
    $current_page = $pager_page_array[$element];
    $last_page = max($pager_total[$element] - 1,0);
  
    $from = min($current_page * $items_per_page + 1, $total_items);
    $to = min($from + $items_per_page - 1, $total_items);
  } else {
    $from = '0';
    $to = '?';
    $total_items = '?';
  }
  
  $range_text = '<span class="pager-item-from">' . $from . '</span>';
  if ($from != $to) {
    $range_text .= '-';
    $range_text .= '<span class="pager-item-to">' . $to . '</span>';
  }
  $total_text = '<span class="pager-item-total">' . $total_items . '</span>';
  
  $id_suffix = (!is_numeric($element) || $element != 0) ? '-' . $element : '';
  $query = drupal_get_query_parameters($_GET, array('q','page'));
  $page_of_styles = $ajax && !$ajax_loaded ? array('display:none') : array();
  
  $page_range_of = array(
    '#type' => 'toolstrip_label',
    '#attributes' => array('id' => 'page-of' . $id_suffix, 'class' => array('page-of'), 'style' => $page_of_styles),
    '#text' => t('!range of !total', array('!range' => $range_text, '!total' => $total_text)),
    '#text_html' => true,
  );
  $pager_prev = array(
    '#type' => 'toolstrip_button',
    '#attributes' => array('id' => 'pager-prev' . $id_suffix, 'class' => array_merge($options['button_classes'], array('pager-prev'))),
    '#class' => array('pager-prev'),
    '#img_left' => true,
  );
  $pager_next = array(
    '#type' => 'toolstrip_button',
    '#attributes' => array('id' => 'pager-next' . $id_suffix, 'class' => array_merge($options['button_classes'], array('pager-next', 'right-tooltip'))),
    '#class' => array('pager-next'),
    '#img_left' => true,
  );
  if ($ajax || $current_page != 0) {
    $pager_prev['#url'] = $_GET['q'];
    $pager_prev['#url_options'] = array('fragment' => 'prev');
    if (!$ajax) {
      $prev_query = ($current_page == 1) ? $query : array_merge($query, array('page' => $current_page - 1));
      $pager_prev['#url_options'] = array('query' => $prev_query);
    } else if (!$ajax_loaded || $current_page == 0) {
      $pager_prev['#attributes']['class'][] = 'ui-disabled';
    }
    $pager_prev['#tip_text'] = t('Previous');
  } else {
    $pager_prev['#disabled'] = true;
  }
  if ($ajax || $current_page != $last_page) {
    $pager_next['#url'] = $_GET['q'];
    $pager_next['#url_options'] = array('fragment' => 'next');
    if (!$ajax) {
      $next_query = array_merge($query, array('page' => $current_page + 1));
      $pager_next['#url_options'] = array('query' => $next_query);
    } else if (!$ajax_loaded || $current_page == $last_page)  {
      $pager_next['#attributes']['class'][] = 'ui-disabled';
    }
    $pager_next['#tip_text'] = t('Next');
  } else {
    $pager_next['#disabled'] = true;
  }
  
  if ((!$ajax || $ajax_loaded) && $options['hide_empty'] == TRUE && $total_items <= $items_per_page) {
    $attributes += array('style' => array('display:none'));
  }

  $items = array();
  $range = $ajax || $total_items > 0 ? $page_range_of : array();
  switch ($options['button_location']) {
    case 'before':
      $items['pager-prev'] = $pager_prev;
      $items['pager-next'] = $pager_next;
      $items['page-range-of'] = $range;
      break;
    case 'outside':
      $items['pager-prev'] = $pager_prev;
      $items['page-range-of'] = $range;
      $items['pager-next'] = $pager_next;
      break;
    default:
      $items['page-range-of'] = $range;
      $items['pager-prev'] = $pager_prev;
      $items['pager-next'] = $pager_next;
      break;
  }
  
  //die(print_r($items, true));
  $element = array(
    '#type' => 'toolstrip',
    '#attributes' => $attributes,
    '#items' => $items,
  );
  return drupal_render($element);
}

function theme_openfit_timeofday($variables = array('element' => array())) {
  $element = $variables['element'];
  $element += array('#autocomplete_path' => FALSE, '#input' => TRUE,  '#maxlength' => 20, '#size' => 60,);
  return theme('textfield', array('element' => $element));
}

function theme_openfit_duration($variables = array('element' => array())) {
  $element = $variables['element'];
  $element += array('#autocomplete_path' => FALSE, '#input' => TRUE,  '#maxlength' => 20, '#size' => 60,);
  return theme('textfield', array('element' => $element));
}

function theme_openfit_measurement($variables = array('element' => array())) {
  $element = $variables['element'];
  $element += array('#autocomplete_path' => FALSE, '#input' => TRUE,  '#maxlength' => 20, '#size' => 60,);
  return theme('textfield', array('element' => $element));
}
