<?php

class ActivityDataTrackAccess {
  /**
   * Each point contains an array of float latitude, longitude.
   */
  const LOCATION = 'location';
  
  /**
   * Each point contains the integer elevation meters.
   */
  const ELEVATION = 'elevation';
  
  /**
   * Each point contains the integer distance meters.
   */
  const DISTANCE = 'distance';
  
  /**
   * Each point contains the integer heart rate beats per minute.
   */
  const HEARTRATE = 'heartrate';
  
  /**
   * Each point contains the integer cadence revolutions per minute.
   */
  const CADENCE = 'cadence';
   
  /**
   * Each point contains the integer power watts.
   */
  const POWER = 'power';
  
   /**
    * Each point contains the float celsius temperature.
    */
  const TEMPERATURE = 'temperature';
  
  /**
   * Each point contains the meters per second speed.
   */
  const SPEED = 'speed';
  
  /**
   * Each point contains the seconds per meters.
   */
  const PACE = 'pace';
  
  /**
   * Construct an accessor for the specified activity.
   */
  function __construct($activity_id, $type = '') {
    $this->activityId = $activity_id;
    $this->type = $type;
    // Included for use with string extractors like potx.
    t('Route');
    t('Elevation');
    t('Distance');
    t('Heart Rate');
    t('Cadence');
    t('Power');
    t('Temperature');
    t('Speed');
    t('Pace');
  }
  
  public static function getAllDataTrackTypes() {
    return array(
      self::LOCATION,
      self::ELEVATION,
      self::DISTANCE,
      self::HEARTRATE,
      self::CADENCE,
      self::POWER,
      self::TEMPERATURE,
      self::SPEED,
    );
  }
  
  public static function delete($activity_id, $type = '') {
    $filename = self::getDatafileName($activity_id, $type);
    unlink($filename);
  }
  
  // TODO: All these track specific functions and the tracks themselves should be defined in the database, except maybe distance, elevation and location
  public static function getMeasurementInfo($activity, $track_type) {
    global $user;
    $units_none = array(
      'unit_id' => 'none',
      'unit_single' => '', 'unit_plural' => '', 'unit_symbol' => '', 
      'unit_decimals' => 0, 'conversion_factor' => 1, 'conversion_offset' => 0);
    switch ($track_type) {
      case self::ELEVATION:
        return OpenFitMeasurement::getConversionInfo(OpenFitUserSetting::get($user->uid, OpenFitUserSetting::TYPE_ELEVATION_UNITS));
      case self::DISTANCE:
        $distance_units = OpenFitUserSetting::get($user->uid, OpenFitUserSetting::TYPE_DISTANCE_UNITS);
        if (isset($activity['category_length_unit'])) $distance_units = $activity['category_length_unit'];
        return OpenFitMeasurement::getConversionInfo($distance_units);
      case self::TEMPERATURE:
        return OpenFitMeasurement::getConversionInfo(OpenFitUserSetting::get($user->uid, OpenFitUserSetting::TYPE_TEMPERATURE_UNITS));
      case self::SPEED:
        return OpenFitMeasurement::getConversionInfo(OpenFitUserSetting::get($user->uid, OpenFitUserSetting::TYPE_SPEED_UNITS));
      case self::PACE: {
        $distance_units = OpenFitUserSetting::get($user->uid, OpenFitUserSetting::TYPE_DISTANCE_UNITS);
        if (isset($activity['category_length_unit'])) $distance_units = $activity['category_length_unit'];
        $distance_info = OpenFitMeasurement::getConversionInfo($distance_units);
        $length_value = 1;
        if (isset($activity['category_length_value'])) $length_value = floatval($activity['category_length_value']);
        $length_value_text = ($length_value != 1 && $length_value != 0) ? $length_value . ' ' : '';
        $distance_info->conversion_factor *= $length_value;
        $distance_info->unit_symbol = '/' . $length_value_text . $distance_info->unit_symbol;
        $distance_info->time = true;
        return $distance_info;
      }
      default:
        return $units_none;
    }
  }
  
  public static function getTitle($track_type) {
    if (isset(self::$trackTypesInfo[$track_type])) return t(self::$trackTypesInfo[$track_type]['title']);
    return t('Data');
  }
  
  public static function getColor($track_type) {
    if (isset(self::$trackTypesInfo[$track_type])) return self::$trackTypesInfo[$track_type]['color'];
    return '#A05050';
  }
  
  public static function getDataSize($track_type) {
    if (isset(self::$trackTypesInfo[$track_type])) return self::$trackTypesInfo[$track_type]['data_size'];
    return strlen(pack(self::getDataFormat($track_type),0));
  }
  
  public static function getDataFormat($track_type) {
    if (isset(self::$trackTypesInfo[$track_type])) return self::$trackTypesInfo[$track_type]['data_format'];
    return 'd';
  }
  
  public static function getTimeDataSize($version) {
    switch ($version) {
      case 1: return 2;
      default: return 4;
    }
  }

  public static function getTimeDataFormat($version) {
    switch ($version) {
      case 1: return 'S';
      default: return 'L';
    }
  }
  
  protected $activityId;
  protected $type;
  
  // TODO: Move this schema into the database.
  protected static $trackTypesInfo = array(
    self::LOCATION => array (
      'id' => 1,
      'version' => 1,
      'data_size' => 8,
      'data_format' => 'f2', // TODO: Should be machine independent ordering to support data portability, always pick one endian type
      'title' => 'Route',
      'color' => '#0080FF',
    ),
    self::ELEVATION => array (
      'id' => 2,
      'version' => 1,
      'data_size' => 4,
      'data_format' => 'f', // TODO: Should be machine independent ordering to support data portability, always pick one endian type
      'title' => 'Elevation',
      'color' => '#AD7118',
    ),
    self::DISTANCE => array (
      'id' => 3,
      'version' => 1,
      'data_size' => 4,
      'data_format' => 'f', // TODO: Should be machine independent ordering to support data portability, always pick one endian type
      'title' => 'Distance',
      'color' => '#204A87',
    ),
    self::HEARTRATE => array (
      'id' => 4,
      'version' => 1,
      'data_size' => 1,
      'data_format' => 'C',
      'title' => 'Heart Rate',
      'color' => '#CE1C2E',
    ),
    self::CADENCE => array (
      'id' => 5,
      'version' => 1,
      'data_size' => 1,
      'data_format' => 'C',
      'title' => 'Cadence',
      'color' => '#9DC744',
    ),
    self::POWER => array (
      'id' => 6,
      'version' => 1,
      'data_size' => 1,
      'data_format' => 'C',
      'title' => 'Power',
      'color' => '#9B5AAD',
    ),
    self::TEMPERATURE => array (
      'id' => 7,
      'version' => 1,
      'data_size' => 2,
      'data_format' => 's', // TODO: This should be machine independent
      'data_multiple' => 10,
      'title' => 'Temperature',
      'color' => '#A0A0A0',
    ),
    self::SPEED => array (
      'title' => 'Speed',
      'color' => '#377AEB',
    ),
    self::PACE => array (
      'title' => 'Pace',
      'color' => '#68D8D8',
    ),
  );
  
  private static function getDatafileName($activity_id, $type) {
    $ext = '.data';
    if (strlen($type) > 0) $ext = '-' . $type . $ext;
    return DRUPAL_ROOT. '/openfit/data_tracks/' . $activity_id . $ext;
  }
  
  protected function getFilename() {
    return self::getDatafileName($this->activityId, $this->type);
  }
  
  const FILE_VERSION = 1;
}

class ActivityDataTrackWriter extends ActivityDataTrackAccess {

  /**
   * Construct a writer for the specified activity.
   */
  function __construct($activity_id, $type = '') {
    parent::__construct($activity_id, $type);
  }
  
  private function processTrackData_default($track_type, &$track, &$start) {
    $type_info = self::$trackTypesInfo[$track_type];
    $data_size = $type_info['data_size'];
    $data_format = isset($type_info['data_format']) ? $type_info['data_format'] : 'C';
    $multiple = isset($type_info['data_multiple']) ? $type_info['data_multiple'] : 1;
    $max_gap = 255;
    
    $out = '';
    $start = 0;
    $len = strlen($track);
    if ($len < 2) {
      $track = '';
      return 0;
    }
    $track_version = unpack('S', $track);
    $track_version = $track_version[1];
    $time_data_size = ActivityDataTrackAccess::getTimeDataSize($track_version);
    $time_data_format = ActivityDataTrackAccess::getTimeDataFormat($track_version);
    $i_s = 2;
    $count = 0;
    if ($i_s < $len) {
      $start = unpack($time_data_format, substr($track, $i_s, $time_data_size));
      $start = $start[1];
    }
    $prior = $start;
    $prior_i_s = 0;
    while ($i_s < $len) {
      $elapsed = unpack($time_data_format, substr($track, $i_s, $time_data_size));
      $elapsed = $elapsed[1];
      $i_s += $time_data_size;
      $diff = $elapsed - $prior;
      if ($diff > $max_gap) {
        $prior_value = unpack($data_format, substr($track, $prior_i_s, $data_size));
        if (count($prior_value) == 1) $prior_value = $prior_value[1];
        $value = unpack($data_format, substr($track, $i_s, $data_size));
        if (count($value) == 1) $value = $value[1];
        if ($multiple != 1) {
          if (is_array($prior_value)) {
            $count_values = count($prior_value);
            for($i = 0; $i < $count_values; $i++) $prior_value[$i+1] *= $multiple;
          } else {
            $prior_value *= $multiple;
          }
          if (is_array($value)) {
            $count_values = count($value);
            for($i = 0; $i < $count_values; $i++) $value[$i+1] *= $multiple;
          } else {
            $value *= $multiple;
          }
        }
        
        if (is_array($value)) {
          $slope = array();
          $count_values = count($value);
          for($i = 0; $i < $count_values; $i++) $slope[$i+1] = ($value[$i+1] - $prior_value[$i+1]) / $diff;
        } else {
          $slope = ($value - $prior_value) / $diff;
        }
        $next_pt = $max_gap;
        while ($diff > $max_gap) {
          $out .= chr($max_gap);
          if (is_array($slope)) {
            $mid_value = array();
            for($i = 0; $i < $count_values; $i++) $mid_value[$i+1] = $prior_value[$i+1] + ($slope[$i+1] * $next_pt);
            $out .= pack($data_format, $mid_value[1], $mid_value[2]);
          } else {
            $mid_value = $prior_value + ($slope * $next_pt);
            $out .= pack($data_format, $mid_value);
          }
          $diff -= $max_gap;
          $next_pt += $max_gap;
          $count ++;
        }
      }
      
      $prior_i_s = $i_s;
      if ($diff > 0 || $count == 0 || $track_type == self::LOCATION) {
        $out .= chr($diff);
        if ($multiple == 1) {
          $out .= substr($track, $i_s, $data_size);
        } else {
          $value = unpack($data_format, substr($track, $i_s, $data_size));
          if (count($value) == 1) $value = $value[1];
          if (is_array($value)) {
            $count_values = count($value);
            for($i = 0; $i < $count_values; $i++) $value[$i] *= $multiple;
            $out .= pack($data_format, $value[1], $value[2]);
          } else {
            $value *= $multiple;
            $out .= pack($data_format, $value);
          }
        }
        $count ++;
      }
      
      $i_s += $data_size;
      $prior = $elapsed;
    }
    $track = $out;
    return $count;
  }
  
  /**
   * Write the data tracks.
   *
   * @param $tracks
   *   An array of data indexed by track type. The array value is a string of packed data.
   *   The data begins with a two byte unsigned version ID (machine order, pack type='S'),
   *   followed by data which varies by version as described below.
   *     0x00: Each data value is represented by a pair of values:
   *     [0]: 4 byte unsigned long ('L') - time offset in seconds from start of workout.
   *     [1]: 1-8 bytes value encoded as with return value from getDataFormat(track type).
   */
  public function writeTracks($tracks) {
    $filename = $this->getFilename();
    
    $file = fopen($filename, 'wb');
    if (!$file) throw new Exception('Could not open file for writing.');

    // Discard tracks whose type we don't know or don't have point data.
    $orig_tracks = $tracks;
    $tracks = array();
    $num_track_points = array();
    $track_start_offset = array();
    foreach ($orig_tracks as $track_type => &$track) {
      // Skip unknown track types.
      if (!isset(self::$trackTypesInfo[$track_type])) continue;
      // Skip tracks with missing data.
      if (!isset($track) || strlen($track) == 0) continue;
      
      $version = unpack('S', $track);
      $version = $version[1];
      $num_points = 0;
      $start = 0;
      switch ($version) {
        case 0:
        case 1:
          $num_points = self::processTrackData_default($track_type, $track, $start);
          break;
      }
      if ($num_points == 0) continue;
      
      $num_track_points[$track_type] = $num_points;
      $tracks[$track_type] = $track;
      $track_start_offset[$track_type] = $start;
    }
    
    $num_tracks = count($tracks);
    
    // Count the track size and byte offset
    $tracks_info = array();
    $track_start = 2 + $num_tracks * 5; // 2 bytes header + (1 byte + 1 int) per track
    foreach ($tracks as $track_type => $track) {
      $type_info = self::$trackTypesInfo[$track_type];
      $num_points = $num_track_points[$track_type];
      $tracks_info[$track_type]['start_byte'] = $track_start;
      $tracks_info[$track_type]['num_points'] = $num_points;
      // Offset start by: 1 byte (version) + 4 bytes (num points) + 4 bytes (time offset) + data points
      $track_start += 9 + $num_points * (1 + $type_info['data_size']);
    }

    // Write the file header
    fwrite($file, chr(self::FILE_VERSION));
    fwrite($file, chr($num_tracks));
    
    // Write the track headers
    foreach ($tracks_info as $track_type => $track_info) {
      $type_info = self::$trackTypesInfo[$track_type];
      fwrite($file, chr($type_info['id']));
      fwrite($file, pack('V', $track_info['start_byte']));
    }

    // Write the track data
    foreach ($tracks_info as $track_type => $track_info) {
      $type_info = self::$trackTypesInfo[$track_type];
      $track = $tracks[$track_type];
      
      $data_format = isset($type_info['data_format']) ? $type_info['data_format'] : 'C';
      $multiple = isset($type_info['data_multiple']) ? $type_info['data_multiple'] : 1;

      fwrite($file, chr($type_info['version']));
      fwrite($file, pack('V', $track_info['num_points']));
      fwrite($file, pack('V', max($track_start_offset[$track_type], 0)));
      fwrite($file, $track);      
    }
    fclose($file);    
  }  
}

class ActivityDataTrackReader extends ActivityDataTrackAccess {

  /**
   * Construct a reader for the specified activity.
   */
  function __construct($activity_id, $type = '') {
    parent::__construct($activity_id, $type);
  }
  
  public function getTrackList() {
    $filename = $this->getFilename();
    
    if (!file_exists($filename)) return array();

    // Read track data
    $file = fopen($filename, 'rb');
    if (!$file) throw new Exception('Could not open file for reading.');
    
    $tracks_info = $this->readTracksInfo($file);
    fclose($file);
    
    return array_keys($tracks_info);
  }
  
  /**
   * Read the data tracks.
   *
   * @param $track_types
   *   An array of track types or null to get all.
   * @param $start
   *   A starting cutoff. Values before this will not be returned.
   * @param $end
   *   An ending cutoff. Values after this will not be returned.
   * @return
   *   An associative array by track type of:
   *   offset[0], value[0] .. offset[n], value[n]
   *   Where offset is the elapsed time since the start of the track.
   */
  public function readTracks($track_types = null, $start = null, $end = null) {
    $filename = $this->getFilename();
    
    if (!file_exists($filename)) return array();

    // Read track data
    $file = fopen($filename, 'rb');
    if (!$file) throw new Exception('Could not open file for reading.');

    $tracks_info = $this->readTracksInfo($file);
    
    if ($track_types == null) $track_types = array_keys($tracks_info);
    $tracks = array();    
    foreach ($track_types as $track_type) {
      if (!isset($tracks_info[$track_type])) continue;
      if (!isset(self::$trackTypesInfo[$track_type])) continue;
      $type_info = self::$trackTypesInfo[$track_type];
      
      fseek($file, $tracks_info[$track_type]);
      // Read the track header
      $header = array();
      $version = ord(fread($file, 1));
      $num_points = unpack('V', fread($file, 4));
      $num_points = $num_points[1];
      $offset = unpack('V', fread($file, 4));
      $offset = $offset[1];
      $header['offset'] = $offset;
      $data_size = $type_info['data_size'];
      $data_format = isset($type_info['data_format']) ? $type_info['data_format'] : 'C';
      $multiple = isset($type_info['data_multiple']) ? $type_info['data_multiple'] : 1;

      // Read the points
      $points = array();
      
      //$tracks[$track_type] = array('header' => $header, 'data' => $points);
      //continue;
      
      $time = 0;
      for ($p = 0; $p < $num_points; $p++) {
        $offset = ord(fread($file, 1));
        $time += $offset;
        $after_end = isset($end) && $time > $end;
        if ($after_end) break; // Early exit
        
        $value = fread($file, $data_size);
        
        $before_start = isset($start) && $time < $start;

        if (!$before_start) {
          if ($data_format == 'C') {
            $value = ord($value);
          } else {
            $value = unpack($data_format, $value);
            if (count($value) == 1) $value = $value[1];
          }
          if ($multiple != 1) $value /= $multiple;
        
          $points[] = $time;
          $points[] = $value;
        }
      }
      $header['total_time'] = $time;
      
      $tracks[$track_type] = array('header' => $header, 'data' => $points);
    }
    
    fclose($file);
    
    return $tracks;
  }
  
  /**
   * Read the file header section and return an associative array of track type > byte offset.
   *
   * @param $file
   *   A file handle.
   */
  private function readTracksInfo($file) {
    // Read the track header
    $version = ord(fread($file, 1));
    $num_tracks = ord(fread($file, 1));

    $type_for_ids = array();
    foreach (self::$trackTypesInfo as $type => $type_info) {
      if (!isset($type_info['id'])) continue;
      $type_for_ids[$type_info['id']] = $type;
    }
    
    // Read the track info
    $tracks_info = array();
    for ($i = 0; $i < $num_tracks; $i++) {
      $type_id = ord(fread($file, 1));
      $start_byte = unpack('V', fread($file, 4));
      if (!isset($type_for_ids[$type_id])) continue;
      
      $start_byte = $start_byte[1];
      $tracks_info[$type_for_ids[$type_id]] = $start_byte;
    }
    
    return $tracks_info;
  }
}
?>