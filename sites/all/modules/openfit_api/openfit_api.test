<?php
class OpenFitActivityDataTrackTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => 'ActivityDataTrack tests.',
      'description' => 'ActivityDataTrack tests.',
      'group' => 'OpenFit',  
    );
  }
  
  public function setUp() {
    parent::setUp(array('openfit_api'));
  }
  
  /**
   * $data is array of [offset-from-start-N][value-N]
   */
  private function getDataTrack($type, $data, $version = 0) {
    $data_format = ActivityDataTrackAccess::getDataFormat($type);
    $time_format = ActivityDataTrackAccess::getTimeDataFormat($version);
    $track = '';
    $track .= pack('S',$version);
    $count = count($data);
    $i = 0;
    while ($i < $count) {
      $time = $data[$i];
      $value = $data[$i+1];
      $track .= pack($time_format, $time);
      if ($type == ActivityDataTrackAccess::LOCATION) {
        $track .= pack($data_format, $value[0], $value[1]);
      } else {
        $track .= pack($data_format, $value);
      }
      $i += 2;
    }
    return $track;
  }

  private function getDataTracks($data_tracks, $version = 0) {
    $tracks = array();
    foreach ($data_tracks as $type => $data) $tracks[$type] = self::getDataTrack($type, $data, $version = 0);
    return $tracks;
  }
  
  function testReadWriteDataTracks() {
    $activity_id = 0;
    $test_data = array(
      ActivityDataTrackAccess::LOCATION => array(5,array(1,2),20,array(3,4)),
      ActivityDataTrackAccess::ELEVATION => array(10,100,30,200,60,300),
      ActivityDataTrackAccess::DISTANCE => array(0,1),
      ActivityDataTrackAccess::HEARTRATE => array(600,2,1000,22),
      ActivityDataTrackAccess::CADENCE => array(0,3),
      ActivityDataTrackAccess::POWER => array(0,4),
      ActivityDataTrackAccess::TEMPERATURE => array(0,8),
    );
    $write_tracks = self::getDataTracks($test_data);
    
    $writer = new ActivityDataTrackWriter($activity_id, 'full');
    $writer->writeTracks($write_tracks);
    
    $reader = new ActivityDataTrackReader($activity_id, 'full');
    $read_tracks = $reader->readTracks();

    // Check read data against original test data
    $test_data[ActivityDataTrackAccess::HEARTRATE] = array(600,2,855,14,1000,22);
    $this->assertTrue(is_array($read_tracks), 'readTracks() returns array');
    $this->assertTrue(count($read_tracks) == count($test_data), 'count(tracks) = ' . count($test_data));
    foreach ($test_data as $type => $data) {
      $track = $read_tracks[$type];
      $this->assertTrue(isset($track), 'has ' . $type . ' track');
      $header = $track['header'];
      $this->assertTrue(isset($header), $type . ' track has header info');
      $track_data = $track['data'];
      $this->assertTrue(isset($track_data), $type . ' track has data');
      
      // Header
      $offset = $data[0];
      $this->assertTrue($header['offset'] == $offset, $type . ' offset = ' . $offset);
      $total_time = $data[count($data)-2] - $offset;
      $this->assertTrue($header['total_time'] == $total_time, $type . ' total time = ' . $total_time);
      
      // Data
      $data_count = count($data);
      $this->assertTrue($data_count == count($track_data), $type . ' count(data) = ' . $data_count);
      
      $i = 0;
      $n = 0;
      while ($i < $data_count) {
        $elapsed = $data[$i] - $offset;
        $value = $data[$i+1];
        $this->assertTrue($elapsed == $track_data[$i], $type . ' elapsed[' . $n . '] = ' . $elapsed);
        if ($type == ActivityDataTrackAccess::LOCATION) {
          $same = true;
          for($j = 0; $j < count($value); $j++) {
            if ($value[$j] != $track_data[$i+1][$j+1]) $same = false;
          }
          $this->assertTrue($same, $type . ' elapsed[' . $n . '] = ' . print_r($value, true));
        } else {
          $this->assertTrue($value == $track_data[$i+1], $type . ' value[' . $n . '] = ' . $value . ', got=' . $track_data[$i+1]);
        }
        $i += 2;
        $n++;
      }
    }
  }  
}

/**
 * Test the service API.
 * This will also test CRUD methods for Activity node including binary data track persistence.
 */
class OpenFitApiServiceTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => 'API service tests.',
      'description' => 'API service tests.',
      'group' => 'OpenFit',  
    );
  }
  
  public function setUp() {
    parent::setUp(array('services','rest_server','openfit_api'));
    module_load_include('inc', 'services', 'services.runtime');
    module_load_include('inc', 'openfit_api', 'openfit_api.activity_service_resource');
    $this->installApiService();
  }
  
  private function installApiService() {
    // ------------------------------------------------------------------------
    // Pulled from openfit_core profile install: openfit_core_install_create_services()
    // ------------------------------------------------------------------------
    $endpoint_name = 'openfitapi_rest';
    $endpoint = new stdClass;
    $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
    $endpoint->api_version = 3;
    $endpoint->name = $endpoint_name;
    $endpoint->server = 'rest_server';
    $endpoint->path = 'openfitapi/api';
    $endpoint->export_type = 0x00; // Avoid exception in ctools_export_crud_save()
    $endpoint->authentication = array(
      'services' => 'services',
    );
    $endpoint->server_settings = array(
      'rest_server' => array(
        'formatters' => array(
          'json' => TRUE,
          'xml' => TRUE,
          'yaml' => FALSE,
          'bencode' => FALSE,
          'jsonp' => FALSE,
          'php' => FALSE,
          'rss' => FALSE,
        ),
        'parsers' => array(
          'application/json' => TRUE,
          'application/x-www-form-urlencoded' => TRUE,
          'application/x-yaml' => TRUE,
          'multipart/form-data' => TRUE,
          'application/vnd.php.serialized' => FALSE,
        ),
      ),
    );
    $endpoint->resources = array(
      'activity' => array(
        'alias' => 'fitnessActivities',
        'operations' => array(
          'index' => array(
            'enabled' => 1,
          ),
          'retrieve' => array(
            'enabled' => 1,
          ),
          'create' => array(
            'enabled' => 1,
          ),
        ),
      ),
      'system' => array(
        'actions' => array(
          'connect' => array(
            'enabled' => 1,
          ),
        ),
      ),
      'user' => array(
        'actions' => array(
          'login' => array(
            'enabled' => 1,
          ),
          'logout' => array(
            'enabled' => 1,
          ),
        ),
      ),
    );
    $endpoint->debug = 0;
    
    services_endpoint_save($endpoint);
    
    // ------------------------------------------------------------------------
    // Code pulled from services_endpoint_callback()
    // ------------------------------------------------------------------------
    $endpoint = services_endpoint_load($endpoint_name);
    $server = $endpoint->server;

    if (function_exists($server . '_server')) {
      // call the server
      services_set_server_info_from_array(array(
        'module'        => $server,
        'endpoint'      => $endpoint_name,
        'endpoint_path' => $endpoint->path,
        'debug'         => $endpoint->debug,
        'settings'      => $endpoint->server_settings[$server],
      ));
      //print call_user_func($server . '_server');
    }
    
    // ------------------------------------------------------------------------
    // Code pulled from RESTServer::handle()
    // ------------------------------------------------------------------------
    services_set_server_info('resource_uri_formatter', array(&$this, 'uriFormatterRESTServer'));
  }

  public function uriFormatterRESTServer($path) {
    $endpoint_name = services_get_server_info('endpoint');
    $endpoint = services_endpoint_load($endpoint_name);
    return url($endpoint->path . '/' . join($path, '/'), array(
      'absolute' => TRUE,
    ));
  }

  /**
   * Check write/read tests for activity fields and data tracks.
   */
  function testServiceCreateRetrieve() {
    $track_data = array(
      ActivityDataTrackAccess::LOCATION => array(0,array(1,2),20,array(3,4)),
      ActivityDataTrackAccess::ELEVATION => array(10,100,30,200,60,300),
      ActivityDataTrackAccess::DISTANCE => array(0,1),
      ActivityDataTrackAccess::HEARTRATE => array(600,2,1000,22),
      ActivityDataTrackAccess::CADENCE => array(0,3),
      ActivityDataTrackAccess::POWER => array(0,4),
      ActivityDataTrackAccess::TEMPERATURE => array(0,5),
    );
    $fields = array(
      'max_speed','duration','clock_duration',
      'calories','elevation_gain','elevation_loss',
      'avg_heartrate','max_heartrate',
      'avg_cadence','max_cadence',
      'avg_power','max_power',
    );
    $activity_fields = array(
      'privacy','type','name','location_name','notes','total_distance'
    );
    $lap_fields = array(
      'type','distance',
    );
    $data = array(
      'start_time' => '2012-01-01T12:00:00',
      'privacy' => 'public',
      'type' => 'Cycling',
      'name' => 'My bike ride',
      'location_name' => 'My location',
      'notes' => 'A test ride',
      'max_speed' => 60,
      'total_distance' => 10000,
      'duration' => 1000,
      'clock_duration' => 1200,
      'calories' => 600,
      'elevation_gain' => 100,
      'elevation_loss' => 200,
      'avg_heartrate' => 20,
      'max_heartrate' => 30,
      'avg_cadence' => 40,
      'max_cadence' => 50,
      'avg_power' => 60,
      'max_power' => 70,
      'laps' => array(
        array(
          'start_time' => '2012-01-01T12:01:00',
          'type' => 'active',
          'max_speed' => 55,
          'distance' => 2000,
          'duration' => 600,
          'clock_duration' => 650,
          'calories' => 250,
          'elevation_gain' => 10,
          'elevation_loss' => 20,
          'avg_heartrate' => 21,
          'max_heartrate' => 31,
          'avg_cadence' => 41,
          'max_cadence' => 51,
          'avg_power' => 61,
          'max_power' => 71,
        ),
        array(
          'start_time' => '2012-01-01T12:11:50',
          'type' => 'active',
          'max_speed' => 60,
          'distance' => 8000,
          'duration' => 400,
          'clock_duration' => 550,
          'calories' => 350,
          'elevation_gain' => 90,
          'elevation_loss' => 180,
          'avg_heartrate' => 22,
          'max_heartrate' => 32,
          'avg_cadence' => 42,
          'max_cadence' => 52,
          'avg_power' => 62,
          'max_power' => 72,
        ),
      ),
    );
    $data = array_merge($data, $track_data);
    
    $response = openfit_api_resource_activity_create($data);
    $this->assertTrue(is_array($response), 'create() returns array');
    $this->assertTrue(is_array($response['uris']), 'returned uris array');
    $this->assertTrue(count($response['uris']) == 1, 'count(uris) = 1');
    $uri = $response['uris'][0];
    $this->assertTrue(strlen($uri) > 0,'uri[0] not empty');
    
    // Get the activity id from the last part of the URL
    $path = explode('/', $uri);
    $nid = $path[count($path)-1];
    
    $activity_info = openfit_api_resource_activity_retrieve($nid);
    
    $track_data[ActivityDataTrackAccess::HEARTRATE] = array(600,2,855,14,1000,22);
    foreach ($track_data as $type => $track) {
      $this->assertTrue(isset($activity_info[$type]), 'has ' . $type . ' track');
      $this->assertTrue($track == $activity_info[$type], $type . ' data = ' . print_r($track, true));
      $this->assertTrue(true, print_r($activity_info[$type], true));
    }
    foreach ($fields as $field) {
      $this->assertTrue($data[$field] == $activity_info[$field], $field . ' = ' . $data[$field]);
    }
    foreach ($activity_fields as $field) {
      $this->assertTrue($data[$field] == $activity_info[$field], $field . ' = ' . $data[$field]);
    }
    if (isset($data['laps'])) {
      $this->assertTrue(isset($activity_info['laps']), 'has laps');
      for($i = 0; $i < count($data['laps']); $i++) {
        $activity_lap = $activity_info['laps'][$i];
        $lap = $data['laps'][$i];
        foreach ($fields as $field) {
          $this->assertTrue($lap[$field] == $activity_lap[$field], 'lap #' . $i . ' ' . $field . ' = ' . $lap[$field]);
        }
        foreach ($lap_fields as $field) {
          $this->assertTrue($lap[$field] == $activity_lap[$field], 'lap #' . $i . ' ' . $field . ' = ' . $lap[$field]);
        }
      }
    }
  }
  
  /**
   * Test code to generate distance track from GPS, total distance, elev gain/loss, avg/max for tracks
   */
  function testCalculatedInfo() {
    $data = array(
      'start_time' => '2012-01-01T12:00:00',
      ActivityDataTrackAccess::LOCATION => array(
        10, array(48.8618354797363,2.33287811279297),
        110, array(52.5185203552246,13.4045648574829),
        210, array(45.474006652832,9.1761589050293)  
      ),
      ActivityDataTrackAccess::ELEVATION => array(0,100,1,110,2,100,50,200,100,150),
      ActivityDataTrackAccess::HEARTRATE => array(0,50,10,100,20,80,30,90),
      ActivityDataTrackAccess::CADENCE => array(0,55,10,105,20,80,30,90),
      ActivityDataTrackAccess::POWER => array(0,60,10,110,20,80,30,90),
    );
    $response = openfit_api_resource_activity_create($data);
    $uri = $response['uris'][0];
    
    // Get the activity id from the last part of the URL
    $path = explode('/', $uri);
    $nid = $path[count($path)-1];
    
    $activity_info = openfit_api_resource_activity_retrieve($nid);
    
    $distance = array(10,0,110,878266.3,210,1719767.8);
    $duration = 200; // seconds
    $total_distance = 1719767.75; // meters
    $max_speed = 8782.66; // meters per second
    $elev_gain = 100; // meters
    $elev_loss = 50; // meters
    $avg_heartrate = 83.3;
    $max_heartrate = 100;
    $avg_cadence = 85.8;
    $max_cadence = 105;
    $avg_power = 88.3;
    $max_power = 110;
    
    $this->assertTrue(isset($activity_info[ActivityDataTrackAccess::DISTANCE]), 'has distance track');
    $distance_track = $activity_info[ActivityDataTrackAccess::DISTANCE];
    $this->assertTrue($distance == $distance_track, 'distance track = ' . print_r($distance, true));
    
    $this->assertTrue($activity_info['duration'] == $duration, 'duration = ' . $duration);
    $this->assertTrue($activity_info['clock_duration'] == $duration, 'clock duration = ' . $duration);
    $this->assertTrue($activity_info['total_distance'] == $total_distance, 'total distance = ' . $total_distance);
    $this->assertTrue($activity_info['max_speed'] == $max_speed, 'max speed = ' . $max_speed);
    $this->assertTrue($activity_info['elevation_gain'] == $elev_gain, 'elevation gain = ' . $elev_gain);
    $this->assertTrue($activity_info['elevation_loss'] == $elev_loss, 'elevation loss = ' . $elev_loss);
    
    $this->assertTrue($activity_info['avg_heartrate'] == $avg_heartrate, 'avg heartrate = ' . $avg_heartrate);
    $this->assertTrue($activity_info['max_heartrate'] == $max_heartrate, 'max heartrate = ' . $max_heartrate);
    $this->assertTrue($activity_info['avg_cadence'] == $avg_cadence, 'avg cadence = ' . $avg_cadence);
    $this->assertTrue($activity_info['max_cadence'] == $max_cadence, 'max cadence = ' . $max_cadence);
    $this->assertTrue($activity_info['avg_power'] == $avg_power, 'avg power = ' . $avg_power);
    $this->assertTrue($activity_info['max_power'] == $max_power, 'max power = ' . $max_power);
  }
  
  //function testServiceRetrieve() {
  //  $this->assertTrue(true, 'true');
  //}
}

?>