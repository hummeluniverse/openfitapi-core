<?php 

class OpenFitUserComment {
  public static function getActivityComments($nid, $node_uid, $count = 5) {
    $query = db_select('comment', 'c');
    
    if (isset($count)) {
      if ($count <= 0) $count = 1;
      $query->range(0, $count);
    }
    $query->innerJoin('field_data_comment_body', 'cb', 'c.cid = cb.entity_id');
    $query->innerJoin('users', 'u', 'u.uid = c.uid');
    $query
      ->fields('c', array('cid', 'nid', 'uid', 'name', 'subject', 'created'))
      ->fields('u', array('picture'))
      ->fields('cb', array('comment_body_value', 'comment_body_format')); //deleted, language
      
    $query->orderBy('created', 'DESC');
    
    $query->condition('c.nid', $nid);
    $query->condition('c.status', 1);
    
    $results = $query->execute();
    $comments = array();
    if ($results) {
      while ($row = $results->fetchAssoc()) {
        $row['node_uid'] = $node_uid;
        $comments[$row['cid']] = (object)$row;
      }
    }
    return $comments;    
  }
  
  /*
   * Get the most recent comments for the user. If user is null the current user is returned.
   * A null count returns all comments for the user.
   */
  public static function getRecent($uid = null, $count = 10) {
    $bypass_access = user_access('bypass node access');
    $deny_access = !user_access('access content');
    $published_only_access = !user_access('view own unpublished content');
    if ($deny_access && !$bypass_access) return null;

    global $user;
    $me = $user->uid;
    if (!isset($uid)) {
      $uid = $me;
    }

    $query = db_select('comment', 'c');

    if (isset($count)) {
      if ($count <= 0) $count = 1;
      $query->range(0, $count);
    }
    $query->innerJoin(OpenFitActivity::TABLE_ACTIVITY, 'a', 'a.nid = c.nid');
    $query->innerJoin(OpenFitActivity::TABLE_ACTIVITY_CATEGORY, 'cat', 'a.activity_category_id = cat.category_id');
    $query->innerJoin('field_data_comment_body', 'cb', 'c.cid = cb.entity_id');
    $query->innerJoin('users', 'u', 'u.uid = c.uid');
    $query
      ->fields('c', array('cid', 'nid', 'uid', 'name', 'subject', 'created'))
      ->fields('u', array('picture'))
      ->fields('cb', array('comment_body_value', 'comment_body_format')) //deleted, language
      ->fields('a', array('activity_start', 'activity_timezone'))
/*
        'activity_id', 'activity_start', 'activity_timezone', 'activity_distance', 'activity_duration', 'activity_clock_duration',
        'activity_elevation_gain', 'activity_elevation_loss', 'activity_location', 'activity_notes', 'activity_calories',
        'activity_max_speed', 'activity_avg_heartrate', 'activity_max_heartrate',
        'activity_avg_cadence', 'activity_max_cadence', 'activity_avg_power', 'activity_max_power',
        'activity_timer_stops',
      ))
*/
      ->fields('cat', array('category_noun'));
/*
        'category_id', 'category_name', 'category_noun', 'category_image_url', 'category_pace_or_speed', 'category_length_value'));
*/
    $query->addExpression("'activity'",'node_type');
    
    $query->orderBy('created', 'DESC');
    
    $query->condition('a.uid', $uid);
    
    if (!$bypass_access) {
      if ($me != $uid) {
        if ($me == 0) {
          // Guests can see comments for public workouts.
          $query->condition('a.activity_privacy', OpenFitActivity::STATUS_PUBLIC);
        } else {
          // Allow authenticated users to see comments for public workouts and their friend's workouts.
          $clause = db_or()
            ->condition('a.activity_privacy', OpenFitActivity::STATUS_PUBLIC)
            ->condition(db_and()
              ->condition('a.activity_privacy', OpenFitActivity::STATUS_FRIENDS)
              ->where('EXISTS (SELECT group_id FROM {' . OpenFitGroup::TABLE_GROUP_MEMBER . '} g WHERE g.group_type=:type AND g.group_id=a.uid AND g.uid=:uid)', 
                  array(':type' => OpenFitGroup::GROUP_TYPE_USER, ':uid' => $me))
            );
          $query->condition($clause);
        }
      }
      if ($published_only_access) {
        $query->condition('a.activity_privacy', OpenFitActivity::STATUS_PUBLIC);
      }
    }
    $query->condition('c.status', 1);
    
    $results = $query->execute();
    $comments = array();
    if ($results) {
      while ($row = $results->fetchAssoc()) {
        $comments[$row['cid']] = (object)$row;
      }
    }
    return $comments;    
  }
}