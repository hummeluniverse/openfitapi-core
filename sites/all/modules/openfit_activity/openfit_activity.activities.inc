<?php

/**
 * Method to construct the activities form.
 */
function openfit_activity_activities_form($form, &$form_state) {
  drupal_page_is_cacheable(FALSE);
  global $user;
  
  // TODO: Read from user preference
  $rows_per_page = 25;
  
  // TODO: Read from user preference
  $columns = array('name', 'start', 'sport', 'distance', 'time', 'avg-speed', 'avg-pace', 'calories');
  $sort_column = 'start';
  $sort_direction = 'desc';
  
  $field_info = OpenFitActivity::getTableFieldInfo();
  $field_info['name']['link'] = TRUE;
  $field_info['name']['link_class'] = 'view-activity';

  $header = OpenFitDataTable::getTableHeader($field_info, $columns, $sort_column, $sort_direction);
  $activities = OpenFitActivity::getActivities($user->uid, null, $header, $rows_per_page);
  $table_rows = OpenFitDataTable::getTableRows($field_info, $columns, $activities);
  $table_class = array('activity-table');
  $table_class[] = $table_rows > 0 ? 'data-table' : 'data-table-empty';

  drupal_add_library('system', 'ui');
  drupal_add_library('system', 'ui.dialog');
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_api') . '/js/jquery.table.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_activity') . '/js/view.activities.js';
  
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    
    'hidden-action-type' => array(
      '#type' => 'container',
      '#attributes' => array('style' => 'display:none'),
      'action-type' => array(
        '#type' => 'select',
        '#default_value' => 'edit',
        '#options' => array('edit' => t('Edit'), 'delete' => t('Delete'), 'view' => t('View')),
      ),
    )
  );
  
  $items = array(
    'add' => array(
      '#type' => 'toolstrip_button',
      '#attributes' => array('id' => 'add-button', 'class' => 'add-button'),
      '#img_left' => TRUE,
      '#disabled' => !user_access('create activity content'),
      '#text' => t('Add'),
      '#url' => drupal_get_path_alias('node/add/activity'),    
    ),
  );
  if (count($table_rows) > 0) {
    $items['edit'] = array(
      '#type' => 'toolstrip_button',
      '#attributes' => array('id' => 'edit-button', 'class' => 'edit-button'),
      '#img_left' => TRUE,
      //'#disabled' => !can_edit_activity($node->nid),
      '#text' => t('Edit'),
      '#url' => '#',
    );
    // TODO: Open a menu allowing user to select privacy settings
    /*
    $items['privacy'] = array(
      '#type' => 'toolstrip_button',
      '#attributes' => array('id' => 'privacy-button'),
      '#left_image' => TRUE,
      '#right_image' => TRUE,
      //'#disabled' => !can_edit_activity($node->nid),
      '#text' => t('Privacy'),
      '#url' => '#',
    );
    */
    $items['delete'] = array(
      '#type' => 'toolstrip_button',
      '#attributes' => array('id' => 'delete-button', 'class' => 'delete-button'),
      '#img_left' => TRUE,
      //'#disabled' => !can_edit_activity($node->nid),
      '#text' => t('Delete'),
      '#url' => 'activity/delete',
    );
  }

  $form['actions']['left-action-container'] = array(
    '#type' => 'toolstrip',
    '#attributes' => array('class' => 'left-action-container'),
    '#items' => $items,
  );
  
  $form['actions']['right-action-container'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('right-action-container')),
    'pager' => array(
      '#type' => 'container',
      'small-pager' => array(
        '#type' => 'small_pager',
        // TODO: For testing pager, remove in production
        '#options' => array('hide_empty' => FALSE),
      ),
    ),    
    /*
    'right-actions' => array(
      '#type' => 'toolstrip',
      '#attributes' => array('class' => 'right-actions'),
      // TODO: Open a popup to set options for this list
      '#items' => array(
        'settings' => array(
          '#type' => 'toolstrip_button',
          '#attributes' => array('id' => 'settings-button', 'class' => array('right-tooltip', 'settings-button')),
          '#img_left' => TRUE,
          '#url' => '#',
          '#tip_text' => t('Settings'),
        ),        
      ),
    ),
    */
  );
  
  $form['table-wrap'] = array(
    '#type' => 'container',
    '#attributes' => array('style' => 'overflow-x:auto;overflow-y:hidden'),
    'activity-table' => array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $table_rows,
      '#empty' => t('No activities'),
      '#attributes' => array('class' => $table_class, 'id' => 'activity-table'),
    ),
  );
  
  $delete_confirm_markup = '<span class="ui-icon ui-icon-alert" style="float:left;"></span>';
  $delete_confirm_markup .= '<span>' . t('Are you sure you want to delete the selected items?') . '</span>';
  $form['delete-confirm'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'delete-confirm', 'title' => t('Confirm delete'), 'style' => 'display:none'),
    'text' => array(
      '#markup' => $delete_confirm_markup,
    ),
  );
  
  // We need a hidden submit button for the actions 
  $form['submit'] = array('#type' => 'submit', '#attributes' => array('style' => 'display: none;'));

  return $form;
}

/**
 * Form processing function.
 */
function openfit_activity_activities_form_submit($form, &$form_state) {
  switch ($form_state['values']['action-type']) {
    case 'delete':
      $nids = array();
      foreach ($form_state['values']['activity-table'] as $key => $value) {
        if ($value) $nids[] = $key;
      }
      OpenFitActivity::deleteActivities($nids);
      break;
  }   
}

