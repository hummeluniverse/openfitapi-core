<?php

function openfit_activity_dashboard_form_get_totals_panel_markup($title, $field_info, $form_data, $form_fields = null) {
  $page = array(
    'title' => array('#markup' => '<h2>' . $title . '</h2>'),
    'summary' => array(
      '#type' => 'container', 
      '#attributes' => array('class' => array('field-block')),
      OpenFitDataField::getFormDataRenderArray($field_info, $form_data, $form_fields)
    ),
  );
  return drupal_render($page);
}

function openfit_activity_dashboard_form_get_week_chart_data() {
  global $user;
  $week_start = OpenFitUserSetting::get($user->uid, OpenFitUserSetting::TYPE_WEEK_START, variable_get('date_first_day', 1));
  $now = new DateTime('now');
  $now = OpenFitMeasurementDateTime::getFirstDayOfWeekDate($week_start, $now);
  $rows = OpenFitActivity::getWeekStatistics(array('distance'), 5, $user->uid);
  $fmt = new IntlDateFormatter(OpenFitUserSetting::getCurrentUserLocale(), IntlDateFormatter::NONE, IntlDateFormatter::NONE, 'UTC');
  $fmt->setPattern(t('M/d'));
  $data = array();
  foreach ($rows as $row) {
    $barData = array('value' => $row['distance']);
    $barData['label'] = strtolower($fmt->format($row['group']));
    if ($row['group'] == $now) {
      $barData['style'] = 'current';
      $barData['color'] = '#3C83BC';
    }
    $data[] = (object)$barData;   
  }
  return $data;
}

function openfit_activity_dashboard_form_get_month_chart_data() {
  global $user;
  $now = new DateTime('now');
  $now = new DateTime($now->format('Y-m') . '-01');
  $rows = OpenFitActivity::getMonthStatistics(array('distance'), 5, $user->uid);
  $fmt = new IntlDateFormatter(OpenFitUserSetting::getCurrentUserLocale(), IntlDateFormatter::NONE, IntlDateFormatter::NONE, 'UTC');
  $fmt->setPattern('MMM');
  $data = array();
  foreach ($rows as $row) {
    $barData = array('value' => $row['distance']);
    $barData['label'] = strtolower(str_replace('.', '', $fmt->format($row['group'])));
    if ($row['group'] == $now) {
      $barData['style'] = 'current';
      $barData['color'] = '#3C83BC';
    }
    $data[] = (object)$barData;    
  }
  return $data;
}

/**
 * Method to construct the activity dashboard form.
 */
function openfit_activity_dashboard_form($form, &$form_state) {
  drupal_page_is_cacheable(FALSE);
  global $user;
  $activities = OpenFitActivity::getActivities($user->uid, null, null, 8);  
  $rendered_nodes = array();
  if (count($activities) > 0) {
    $nids = array();
    foreach ($activities as $activity) $nids[] = $activity->nid;
    $nodes = node_load_multiple($nids);
    $rendered_nodes = node_view_multiple($nodes);
  } else {
    $rendered_nodes[] = array(
      '#markup' => t('You haven\'t recorded an activity yet.'),
    );
  }
  
  $week_summary_data = OpenFitActivity::getWeekSummaryInfo($user->uid);
  $month_summary_data = OpenFitActivity::getMonthSummaryInfo($user->uid);
  $summary_fields = array('distance', 'duration', 'calories', 'avg-speed', 'avg-pace');
  $summary_field_info = OpenFitActivity::getActivitySummaryFieldInfo();
  $summary_data = OpenFitDataTable::getTableRows($summary_field_info, $summary_fields,
    array($week_summary_data, $month_summary_data));
  
  $actions = array();
  if (node_access('create', OpenFitActivity::NODE_TYPE_ACTIVITY)) {
    $actions['add'] = array(
      '#type' => 'toolstrip_button',
      '#attributes' => array('id' => 'add-button', 'class' => 'add-button'),
      '#img_left' => true,
      '#text' => t('Add'),
      '#url' => drupal_get_path_alias('node/add/activity'),    
    );
    $actions['import'] = array(
      '#type' => 'toolstrip_button',
      '#attributes' => array('id' => 'import-button', 'class' => 'import-button'),
      '#img_left' => true,
      //'#disabled' => !file_import_types_exist(),
      '#text' => t('Import'),
      '#url' => 'activity/import',
    );
  }

  $form['#attributes'] = array('class' => array('gpanel', 'two-66-33'));
  
  $form['item-list'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('region', 'region-two-66-33-first')),
    'header' => array(
      '#type' => 'container',
      'action-bar' => array(
        '#type' => 'container',
        '#attributes' => array('id' => 'action-bar', 'class' => array('clearfix')),
        'actions' => array(
          '#type' => 'toolstrip',
          '#attributes' => array('id' => 'actions'),
          '#items' => $actions
        ),
      ),
      'title' => array(
        '#markup' => '<h2>' . t('Recent activities') . '</h2>',
      ),
    ),
    'items' => array(
      '#type' => 'container',
      '#attributes' => array('class' => array('recent-activities')),
      'items' => $rendered_nodes,
    ),
  );
    
  $this_week_markup = openfit_activity_dashboard_form_get_totals_panel_markup(
    t('This Week'), $summary_field_info, $summary_data[0], $summary_fields);
    
  $this_month_markup = openfit_activity_dashboard_form_get_totals_panel_markup(
    t('This Month'), $summary_field_info, $summary_data[1], $summary_fields);
  
  $form['sidebar'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('info-sidebar', 'region', 'region-two-66-33-second')),
    'totals' => array(
      'this-week' => array(
        '#type' => 'container',
        'block' => array(
          '#markup' => $this_week_markup,
        ),
        'chart' => array(
          '#type' => 'mini_bar_chart',
          '#attributes' => array('id' => 'this-week-chart'),
          '#title' => t('Distance'),
          '#data' => openfit_activity_dashboard_form_get_week_chart_data(),
          '#options' => array(
            'bars' => array('color' => '#9BC642'),
          ),
        ),
      ),
      'this-month' => array(
        '#type' => 'container',
        'block' => array(
          '#markup' => $this_month_markup,
        ),
        'chart' => array(
          '#type' => 'mini_bar_chart',
          '#attributes' => array('id' => 'this-month-chart'),
          '#title' => t('Distance'),
          '#data' => openfit_activity_dashboard_form_get_month_chart_data(),
          '#options' => array(
            'bars' => array('color' => '#9BC642'),
          ),
        ),
      ),
    ),
  );
  return $form;
}