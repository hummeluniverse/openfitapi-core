<?php

/**
 * Method to construct the reports form.
 */
function openfit_activity_reports_form($form, &$form_state) {
  drupal_page_is_cacheable(FALSE);
  global $user;
  global $base_url;
  
  /* TODO: Put this in a drupal element - also JS attach below */
  $ajaxurl = $base_url . '/openfit/api/?op=get_report_chart_data';

  $form = array();  
  $form['chart'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'activity-reports-chart', 'class' => array('chart'), 'data-dataurl' => $ajaxurl),
    'top-bar' => array(
      '#type' => 'container',
      '#attributes' => array('class' => array('top-bar')),
      'left-actions' => array(
        '#type' => 'container',
        '#attributes' => array('class' => array('left-actions')),
        'actions' => array(
          '#type' => 'toolstrip',
          '#attributes' => array('class' => array('chart', 'metrics')),
          '#items' => array(
            'metric-selector' => array(
              '#type' => 'toolstrip_button',
              '#attributes' => array('class' => array('chart-metric', 'metric-selector', 'flat-btn')),
              '#img_right' => true,
              '#text' => ' ',
              '#popup' => array(
                'menu' => array('#markup' => ''),
              ),
              '#popup_class' => array('fill-width'),
            ),
          )
        ),
      ),
      
      'right-actions' => array(
        '#type' => 'container',
        '#attributes' => array('class' => array('right-actions')),
        'actions' => array(
          '#type' => 'toolstrip',
          '#attributes' => array('class' => array('settings')),
          '#items' => array(
            'zoom-out-button' => array(
              '#type' => 'toolstrip_button',
              '#attributes' => array('class' => array('ui-disabled', 'right-tooltip', 'flat-btn', 'zoom-out-button')),
              '#img_left' => true,
              '#tip_text' => t('Fit to window'),
            ),
          ),
        ),
      ),
    ),
    'chart-canvas-container' => array(
      '#type' => 'container',
      '#attributes' => array('class' => array('chart-canvas-container')),
    ),
    'bottom-bar' => array(
      '#type' => 'container',
      '#attributes' => array('class' => array('bottom-bar')),
      'center' => array(
        '#type' => 'container',
        '#attributes' => array('class' => array('center-outer')),
        'actions' => array(
          '#type' => 'toolstrip',
          '#attributes' => array('class' => array('xaxis', 'center-inner')),
          '#items' => array(
            'xaxis-selector' => array(
              '#type' => 'toolstrip_button',
              '#attributes' => array('class' => array('xaxis-selector', 'flat-btn')),
              '#img_right' => true,
              '#text' => ' ',
              '#popup' => array(
                'menu' => array('#markup' => ''),
              ),
              '#popup_class' => array('fill-width'),
            ),
          ),
        ),
      ),
      'spacer' => array('#markup' => '<div style="height:1.0em;padding-bottom:8px">&nbsp;</div>'),
    ),
  );

  drupal_add_library('system', 'ui');
  drupal_add_library('system', 'ui.dialog');
  
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_activity') . '/js/view.reports.js';  
  /* TODO: Move this into chart element */
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_ui') . '/js/flot/jquery.flot.min.js';  
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_ui') . '/js/jquery.openfit_popupmenus.js';  
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_ui') . '/js/jquery.openfit_chartbase.js';  
  $form['#attached']['js'][] = drupal_get_path('module', 'openfit_ui') . '/js/jquery.openfit_activityreportchart.js';  
  
  return $form;
}
