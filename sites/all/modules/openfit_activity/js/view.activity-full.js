(function($) {

  $(document).ready(function() {
    var chart = $('#activity-chart');
    chart.openfit_activitydetailchart();
    
    var ajaxUrl = chart.attr('data-dataurl');
    $.ajax({
      url: ajaxUrl,
      success: function(data, textStatus, jqXHR) {
        var dataTracks = {};
        var defaultTrack = null;
        for (var trackId in data) {
          dataTracks[trackId] = data[trackId];
          if (data[trackId]['default'] != null && data[trackId]['default']) defaultTrack = trackId;
        }
        if (defaultTrack != null) chart.openfit_activitydetailchart('setMetric', defaultTrack);
        chart.openfit_activitydetailchart('setDataTracks', dataTracks);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        // TODO: Handle this by setting text in chart div and error icon
        alert('error code: ' + jqXHR.status + ': ' + jqXHR.responseText);
      },
    });
  });
})(jQuery);
