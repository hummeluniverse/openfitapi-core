(function($) {
  $(document).ready(function() {
    $('#delete-confirm').dialog({
      autoOpen: false,
			resizable: false,
      width: 520,
			height: 140,
			modal: true,
			buttons: [
        {
          text: Drupal.t('Yes'),
          click: function() {
            $(this).dialog('close'); 
            window.location=$('#delete-button a').attr('href');
          }
        },
        {
          text: Drupal.t('No'),
          click: function() { $(this).dialog('close'); }
        }
      ],
		});
    
    $('#delete-button a').click(function(event) {
      $('#delete-confirm').dialog('open');
      event.preventDefault();
    });
  });
})(jQuery);