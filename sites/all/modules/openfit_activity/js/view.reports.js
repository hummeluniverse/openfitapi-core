(function($) {
  $(document).ready(function() {  
    var chart = $('#activity-reports-chart');
    chart.openfit_activityreportchart();
    
    var ajaxUrl = chart.attr('data-dataurl');
    $.ajax({
      url: ajaxUrl,
      success: function(data, textStatus, jqXHR) {
        try {
          data = jQuery.parseJSON(data);
          chart.openfit_activityreportchart('setMetricItems',data.metrics.items,data.metrics['default']);
          chart.openfit_activityreportchart('setXAxisItems',data.groupby.items,data.groupby['default']);
          chart.openfit_activityreportchart('setData',data.data);
        } catch(err) {
          // TODO: Handle this by setting text in chart div and error icon
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        // TODO: Handle this by setting text in chart div and error icon
        alert('error code: ' + jqXHR.status + ': ' + jqXHR.responseText);
      },
    });
  });
})(jQuery);
