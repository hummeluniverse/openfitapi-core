<?php

/**
 * Returns HTML for toolstrip.
 *
 * @param $variables
 *   An associative array containing:
 *   - attributes: Class attributes for the toolstrip.
 */
function theme_toolstrip($variables = array()) {
  openfit_ui_add_default_attributes($variables, 'toolstrip', 'toolstrip');
  
  drupal_add_css(drupal_get_path('module', 'openfit_ui') . '/css/toolstrip.css', array('type' => 'file'));
  
  $output = '<ul ' . drupal_attributes($variables['attributes']) . '>';
  $output .= drupal_render_children($variables['items']);
  $output .= '</ul>';
  return $output;
}

/**
 * Returns HTML for toolstrip button.
 *
 * @param $variables
 *   An associative array containing:
 *   - attributes: Class attributes for the item.
 *   - text: Button text.
 *   - text_html: true if the text contains HTML.
 *   - img_right: true if an image is shown to the right of the text.
 *   - img_left: true if an image is shown to the left of the text.
 *   - disabled: true if the button is disabled.
 *   - url: button URL.
 *   - url_options: URL options.
 *   - popup: Popup menu.
 *   - popup_class: Options for popup menu.
 *   - tip_text: Tooltip text
 */
function theme_toolstrip_button($variables = array()) {
  openfit_ui_add_default_attributes($variables, 'toolstrip-button', 'toolstrip-button');
  
  if ($variables['img_left'] && $variables['img_right']) $variables['attributes']['class'][] = 'two-btns';
  if (strlen($variables['text']) == 0) $variables['attributes']['class'][] = 'no-text';
  if ($variables['disabled']) $variables['attributes']['class'][] = 'ui-disabled';
  
  $id = $variables['attributes']['id'];
  $output = '<li ' . drupal_attributes($variables['attributes']) . '>';
  
  if (!$variables['disabled'] && strlen($variables['tip_text']) > 0) {
    $output .= '<div class="tooltip-wrap"><div class="tooltip"><div class="tooltip-body">' . 
      htmlentities($variables['tip_text'], ENT_COMPAT, 'UTF-8') . 
      '</div><div class="tooltip-stem"></div></div></div>';
  }
  
  $markup = $variables['text'];
  if (strlen($markup) > 0) {
    if (!$variables['text_html']) $markup = htmlentities($markup, ENT_COMPAT, 'UTF-8');
    $markup = "<span class='btn-label'>$markup</span>";
  }
  if ($variables['img_left']) $markup = '<div class="btn-img img-left"></div>' . $markup;
  if ($variables['img_right']) $markup = $markup . '<div class="btn-img img-right"></div>';
  
  if (!$variables['disabled'] && isset($variables['url'])) {
    $url_options = array('html' => TRUE);
    if (count($variables['url_options']) > 0) {
      $url_options = array_merge($url_options, $variables['url_options']);
    }
    $markup = "<div class='wrap'>$markup</div>";
    $output .= l($markup, $variables['url'], $url_options);
  } else {
    $output .= "<div class='wrap'>$markup</div>";
  }
  
  if (!$variables['disabled'] && count($variables['popup']) > 0) {
    $popup = drupal_render_children($variables['popup']);
    $variables['popup_class'][] = 'toolstrip-popup-wrap';
    $classes = implode($variables['popup_class'], ' ');
    $output .= "<div id='$id-popup' class='$classes' style='display:none'><div class='toolstrip-popup'>$popup</div></div>";
    
    drupal_add_js(drupal_get_path('module', 'openfit_ui') . '/js/jquery.openfit_toolstrip.js');
    
    $js = "
(function($) {
  $(document).ready(function() {
    $('#$id').live('click',function(event) {
      $('.toolstrip-popup',this).css('minWidth',$(this).width());
      $(this).toggleClass('popup-open');
      $('#$id-popup').toggle();
    });
  });
})(jQuery);
";
    drupal_add_js($js, array('type' => 'inline'));

  }
  $output .= '</li>';
  
  return $output;
}

/**
 * Returns HTML for toolstrip label.
 *
 * @param $variables
 *   An associative array containing:
 *   - attributes: Class attributes for the item.
 *   - text: Label text.
 *   - text_html: true if the text contains HTML.
 */
function theme_toolstrip_label($variables = array()) {
  openfit_ui_add_default_attributes($variables, 'toolstrip-label', 'toolstrip-label');
  
  if (strlen($variables['text']) == 0) $variables['attributes']['class'][] = 'no-text';
  
  $output = '<li ' . drupal_attributes($variables['attributes']) . '>';
  $markup = $variables['text'];
  if (!$variables['text_html']) $markup = htmlentities($markup, ENT_COMPAT, 'UTF-8');
  $output .= "$markup</li>";
  
  return $output;
}

/**
 * Returns HTML for jquery AJAX selection list.
 *
 * The pages parameter is an array indexed by page id.
 *
 * @param $variables
 *   An associative array containing:
 *   - attributes: Class attributes for the entire pager.
 *   - items: List items.
 */
function theme_jquery_select_list($variables = array()) {
  openfit_ui_add_default_attributes($variables, 'select-list', array('select-list', 'item-list'));
  $id = $variables['attributes']['id'];
  $output = '<ul ' . drupal_attributes($variables['attributes']) . '>';
  foreach ($variables['items'] as $item_id => $item) {
    $attributes = '';
    if (isset($item['#attributes'])) $attributes = drupal_attributes($item['#attributes']);
    $output .= "<li $attributes>" . drupal_render_children($item) . '</li>';
  }
  $output .= '</ul>';
  
  drupal_add_css(drupal_get_path('module', 'openfit_ui') . '/css/toolstrip.css', array('type' => 'file'));
  
    $js = "
(function($) {
  $(document).ready(function() {
    $('#$id li a').live('click',function(event) {
      var me = $(this);
      $.ajax({url: me.attr('href'),
        success:function(){
          $('li', me.closest('ul')).removeClass('item-selected');
          me.closest('li').addClass('item-selected');
        }
      });
      event.preventDefault();
    });
  });
})(jQuery);
";
    drupal_add_js($js, array('type' => 'inline'));
  
  return $output;
}

/**
 * Returns HTML for jquery tabs.
 *
 * The pages parameter is an array indexed by page id.
 *
 * @param $variables
 *   An associative array containing:
 *   - attributes: Class attributes for the entire pager.
 *   - pages: An array of pages.
 */
function theme_jquery_tabs($variables = array()) {
  $pages = $variables['pages'];
  if (count($pages) == 0) return;
  
  openfit_ui_add_default_attributes($variables, 'tabs');
  
  
  $id = $variables['attributes']['id'];
  $output = '<div ' . drupal_attributes($variables['attributes']) . '>';
    
  $tabs_markup = '';
  $pages_markup = '';
  foreach ($pages as $page_id => $page_info) {
    $tab = $page_info['#tab'];
    $tabs_markup .= "<li><a href='#$page_id'>$tab</a></li>";
    $page_markup = drupal_render_children($page_info);
    $pages_markup .= "<div id='$page_id'>$page_markup</div>";
  }
	$output .= "<ul>$tabs_markup</ul>";

  if (count($variables['post_tabs']) > 0) $output .= drupal_render_children($variables['post_tabs']);
 
  $output .= $pages_markup;
  
  $output .= '</div>';

  drupal_add_library('system', 'ui.tabs');
  $js = "(function($) { $(document).ready(function() { $('#$id').tabs();";
  if (count($variables['pre_tabs']) > 0) $js .= "$('#$id .ui-tabs-nav').before('" . addslashes(drupal_render_children($variables['pre_tabs'])) . "');";
  $js .= "});})(jQuery);";
  drupal_add_js($js, array('type' => 'inline'));

  return $output;
}

/**
 * Returns HTML for a mini bar chart.
 *
 * @param $variables
 *   An associative array containing:
 *   - attributes: Class attributes for the entire pager.
 *   - title: An optional title to display under the chart.
 *   - data: Array of data passed through javascript to chart.
 *   - options: Associative array of display options passed to javascript chart.
 *     - bars: Array of options for bars.
 *     - xAxis: Array of options for xAxis.
 */
function theme_mini_bar_chart($variables = array()) {
  openfit_ui_add_default_attributes($variables, 'mini-chart', 'mini-chart');
  
  $id = $variables['attributes']['id'];
  $output = '<div ' . drupal_attributes($variables['attributes']) . '>';
  $output .= "<div class='chart-canvas'></div>";
  if (!empty($variables['title'])) $output .= '<div class="label">' . $variables['title'] . '</div>';
  $output .= '</div>';

  drupal_add_js(drupal_get_path('module', 'openfit_ui') . '/js/mini-bar-chart.js');
  
  if (isset($variables['data'])) {
    $options = $variables['options'];
    $data = $variables['data'];
    drupal_add_js(array(
      'miniChart' => array($id => array('options' => $options, 'data' => $data))
    ), 'setting');
    
    $js = "
(function($) {
  $(document).ready(function() {
    var e=$('#$id .chart-canvas').get()[0];
    var c=new MiniBarChart(e);
    var d=Drupal.settings.miniChart['$id'];
    jQuery.extend(true, c, d.options);
    c.setBarData(d.data);
  });
})(jQuery);
";
    drupal_add_js($js, array('type' => 'inline'));
  }
  return $output;
}

/**
 * Add id and classes to attributes.
 */
function openfit_ui_add_default_attributes(&$variables, $id, $classes = null) {
  $variables['attributes'] += array('id' => $id);
  $variables['attributes']['id'] = drupal_html_id($variables['attributes']['id']);
  if (isset($variables['attributes']['class']) && is_string($variables['attributes']['class'])) {
    $variables['attributes']['class'] = array($variables['attributes']['class']);
  }
  if (!isset($variables['attributes']['class'])) $variables['attributes']['class'] = array();
  if (isset($classes)) {
    if (is_array($classes)) {
      foreach ($classes as $class) $variables['attributes']['class'][] = $class;
    } else {
      $variables['attributes']['class'][] = $classes;
    }
  }
}
