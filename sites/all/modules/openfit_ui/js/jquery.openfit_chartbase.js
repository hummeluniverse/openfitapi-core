(function($) {
  var ChartBase = {
    _init: function() {
    },
    setMetricItems: function(items,selected) {
      this.options.metricItems = items;
      var metrics = [];
      for(var i = 0; i < items.length; i++) {
        var item = {
          id:items[i].id,name:items[i].name,measurement:items[i].measurement};
        var m = item.measurement;
        if (m != null) {
          item.units = m.unit_symbol;
          m.f = m.conversion_factor != null ? m.conversion_factor : 1;
          m.o = m.conversion_offset != null ? m.conversion_offset : 0;
        }
        item.html = this._metricItemHtml(items[i].name,item.units,items[i].color)
        metrics.push(item);
      }
      this._metricWidget.openfit_popupselectmenu('setItems',metrics);
      this.setMetric(selected);
    },
    setMetric: function(metric) {
      this.options.metric = metric;
      this._metricWidget.openfit_popupselectmenu('setSelected',this.options.metric);      
      this._refreshChartData();
    },    
    setXAxisItems: function(items,selected) {
      var xAxis = [];
      for(var i = 0; i < items.length; i++) {
        xAxis.push({id:items[i].id,name:items[i].name,html:this._xAxisItemHtml(items[i].name,items[i].units)});
      }
      this._xAxisWidget.openfit_popupselectmenu('setItems',xAxis);
      this.setXAxis(selected);
    },
    setXAxis: function(selected) {
      var opts = this.options;
      this.convertZoomRange(selected);      
      opts.xAxis = selected;
      this._xAxisWidget.openfit_popupselectmenu('setSelected',opts.xAxis);      
      this._refreshChartData();
    },    
    setZoomRange: function(range) {
      this._zoomRange = range;
      if (range != null) {
        this._zoomOutButton.removeClass('ui-disabled');
      } else {
        this._zoomOutButton.addClass('ui-disabled');
      }
    },
    _onXAxisChanged: function(selected) {
      this.setXAxis(selected);
    },
    _onMetricChanged: function(selected) {
      this.setMetric(selected);
    },
    _onZoomOutClicked: function() {
      this.setZoomRange(null);
      this._refreshChartData();
    },
    _onChartSelectionChanged: function(from,to) {
      var opts = this.options;
      var z = this._zoomRange;
      if (z == null || z[0] != from || z[1] != to) {
        this.setZoomRange([parseFloat(from),parseFloat(to)]);
        this._refreshChartData();
      }
    },
    _getSelectedMetricItem: function() {
      var metric = this.options.metric;
      var items = this.options.metricItems;
      var numItems = items.length;
      for (var i = 0; i < numItems; i++) {
        if (items[i].id == metric) return items[i];
      }
      return null;
    },    
    _metricItemHtml: function(name,units,color) {
      var html = '<span class="metric-name">' + name + '</span>';
      if (color != null && color.length > 0) html = '<div class="metric-color" style="color:' + color + '"><span>&bull;</span></div>' + html;
      if (units != null && units.length > 0) html += '<span class="metric-units">' + units + '</span>';
      return html;
    },
    _xAxisItemHtml: function(name,units) {
      var html = '<span class="xaxis-name">' + name + '</span>';
      if (units != null && units.length > 0) html += '<span class="xaxis-units">' + units + '</span>';
      return html;
    },
    _getTickFormatter: function(measurement) {
      if (measurement == null) return null;
      var me = this;
      if (measurement.time != null) return function(value,axis) { return me._tickFormatterTime(value,axis); };
      if (measurement.f == 1 && measurement.o == 0) return null;
      return function(value,axis) {
        value = me._convertValue(measurement,value);
        var decimals = axis.tickDecimals;
        if (measurement.unit_decimals != null) decimals = Math.max(decimals,measurement.unit_decimals);
        return value.toFixed(decimals);
      };
    },
    _getTickGenerator: function(measurement) {
      if (measurement == null) return null;
      if (measurement.time != null) return null; // TODO: Time tick generator
      if (measurement.f == 1 && measurement.o == 0) return null;
      var me = this;
      return function(axis) { return me._tickGenerator(measurement,axis); };
    },
    _tickGenerator: function(measurement,axis) {
      var min = this._convertValue(measurement,axis.min);
      var max = this._convertValue(measurement,axis.max);
      
      var opts = axis.options;
              
      // estimate number of ticks
      var canvasWidth = this._chartContainer.width();
      var canvasHeight = this._chartContainer.height();
      var noTicks;
      if (typeof opts.ticks == 'number' && opts.ticks > 0) {
          noTicks = opts.ticks;
      } else {
        // heuristic based on the model a*sqrt(x) fitted to some data points that seemed reasonable
        noTicks = 0.3 * Math.sqrt(axis.direction == 'x' ? canvasWidth : canvasHeight);
      }

      var delta = (max - min) / noTicks, size, generator, unit, formatter, i, magn, norm;
      // pretty rounding of base-10 numbers
      var maxDec = opts.tickDecimals;
      var dec = -Math.floor(Math.log(delta) / Math.LN10);
      if (maxDec != null && dec > maxDec) dec = maxDec;

      magn = Math.pow(10, -dec);
      norm = delta / magn; // norm is between 1.0 and 10.0
      
      if (norm < 1.5) {
        size = 1;
      } else if (norm < 3) {
        size = 2;
        // special case for 2.5, requires an extra decimal
        if (norm > 2.25 && (maxDec == null || dec + 1 <= maxDec)) {
          size = 2.5;
          ++dec;
        }
      } else if (norm < 7.5) {
        size = 5;
      } else {
        size = 10;
      }
      
      size *= magn;
      
      if (opts.minTickSize != null && size < opts.minTickSize) size = opts.minTickSize;
        
      axis.tickDecimals = Math.max(0, maxDec != null ? maxDec : dec);
      axis.tickSize = opts.tickSize || size;

      var ticks = [];

      // spew out all possible ticks        
      var start = axis.tickSize * Math.floor(min / axis.tickSize),i = 0, v = Number.NaN, prev;
      do {
        prev = v;
        v = start + i * axis.tickSize;
        ticks.push(this._convertValueInvert(measurement,v));
        ++i;
      } while (v < max && v != prev);
      return ticks;
    },
    _tickFormatterTime: function (value, axis) {
			sign = value < 0 ? '-' : '';
      // TODO: Handle scenario where axis is zoomed in sufficiently to show
      // fractions of seconds. For now, just handle whole seconds
			value = Math.abs(value);
			var hours = Math.floor(value/3600);
			value -= hours*3600;
			var minutes = Math.floor(value/60);
			value -= minutes*60;
      var mult = 1;
      if (axis.tickDecimals > 0) mult = Math.pow(10,axis.tickDecimals);
			var seconds_fraction = Math.round(value*mult)/mult;
      var seconds = Math.floor(seconds_fraction);
      seconds_fraction -= seconds;
			value -= seconds;
      if (seconds >= 60) {
        seconds -= 60;
        minutes++;
        if (minutes >= 60) {
          minutes -= 60;
          hours++;
        }
      }
			
			var hour_digits = 1;
		  var minute_digits = 2;
			var second_digits = 2;
		  hours = (hours > 0) ? this._zeroPad(hours,hour_digits)+':' : '';
			minutes = (minutes >= 0 || hours >= 0) ? this._zeroPad(minutes,minute_digits)+':' : '';
			seconds = (seconds >= 0 || minutes >= 0 || hours >= 0) ? this._zeroPad(seconds,second_digits) : '';
      seconds_fraction = (axis.tickDecimals > 0) ? seconds_fraction.toFixed(axis.tickDecimals).substr(1) : '';
			return sign + hours + minutes + seconds + seconds_fraction;
		},
    _convertValue: function(measurement,value) {
      if (measurement == null) return value;
      if (measurement.f != 0 && measurement.f != 1) value /= measurement.f;
      value += measurement.o;
      return value;
    },
    _convertValueInvert: function(measurement,value) {
      if (measurement == null) return value;
      if (measurement.f != 0 && measurement.f != 1) value *= measurement.f;
      value -= measurement.o;
      return value;
    },
    _calcMinMaxAvg: function(points,xRange,min,max) {
      if (min == null) min = +Infinity;
      if (max == null) max = -Infinity;
      var avg = 0;
      var avgTime = 0;
      var x,y;
      var addedFirst = false, addedLast = false;
      var numPoints = points.length;
      
      for (var i = 0; i < numPoints; i++) {        
        y = points[i][1];
        if (i > 0) {
          var t = points[i][0]-points[i-1][0];
          avgTime += t;
          avg += t*y;
        }
        if (xRange != null) {
          x = points[i][0];
          if (x < xRange[0]) continue;
          if (!addedFirst) {
            // Add the point immediately before xRange start.
            addedFirst = true;
            if (i > 0) {
              y = points[i-1][1];
              if (y < min) min = y;
              if (y > max) max = y;
            }
          }
          if (x > xRange[1]) {
            if (!addedLast) {
              // Add the point immediately after xRange end.
              addedLast = true;
              if (i < numPoints-1) {
                y = points[i+1][1];
                if (y < min) min = y;
                if (y > max) max = y;
              }
            }
            continue;
          }
        }
        if (y < min) min = y;
        if (y > max) max = y;
      }
      if (min == +Infinity) min = 0;
      if (max == -Infinity) max = 0;
      if (avgTime > 0) avg /= avgTime;
      return {min:min,max:max,avg:avg};
    },    
    _zeroPad: function(n,l){n+='';while(n.length<l)n='0'+n;return n},
    _refreshChartData: function () {},
    _parseColor: function(hex) {
      var r, g, b;
      if (hex.substring(0, 1) == "#") hex = hex.substring(1);
      if (hex.length == 3) {
        r = hex.substring(0, 1); r = r + r;
        g = hex.substring(1, 2); g = g + g;
        b = hex.substring(2, 3); b = b + b;
      } else if (hex.length == 6) {
        r = hex.substring(0, 2);
        g = hex.substring(2, 4);
        b = hex.substring(4, 6);
      }
      r = parseInt(r, 16);
      g = parseInt(g, 16);
      b = parseInt(b, 16);
      return {r:r,g:g,b:b};
    },
    _setAlpha: function(color, alpha) {
      var rgb = this._parseColor(color);
      return 'rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ',' + alpha + ')';
    },
    // Ratio is between 0 and 1 
    _changeColor: function(color, ratio, darker) { 
      var difference = Math.round(ratio * 255) * (darker ? -1 : 1), 
          minmax = darker ? 0 : 255,
          minmaxfunc = darker ? Math.max : Math.min, 
          decimal = color.replace( 
              /^#?([a-z0-9][a-z0-9])([a-z0-9][a-z0-9])([a-z0-9][a-z0-9])/i, 
              function() { 
                  return parseInt(arguments[1], 16) + ',' + 
                      parseInt(arguments[2], 16) + ',' + 
                      parseInt(arguments[3], 16); 
              } 
          ).split(/,/); 
      return [ 
          '#', 
          this._zeroPad(minmaxfunc(parseInt(decimal[0], 10) + difference, minmax).toString(16), 2), 
          this._zeroPad(minmaxfunc(parseInt(decimal[1], 10) + difference, minmax).toString(16), 2), 
          this._zeroPad(minmaxfunc(parseInt(decimal[2], 10) + difference, minmax).toString(16), 2) 
      ].join(''); 
    },    
    _zoomRange:null,    
  };
  $.widget("ui.openfit_chartbase", ChartBase);
})(jQuery);
