(function($) {
  var ActivityDetailChart = {
    _init: function() {
      var me = this;
      var opts = this.options;
      
      this._zoomOutButton = $('.zoom-out-button', this.element);
      this._zoomOutButton.click(function(event) { me._onZoomOutClicked(); event.preventDefault(); });
    
      this._metricWidget = $('.toolstrip.metrics .chart-metric', this.element);
      this._metricWidget.openfit_popupselectmenu();
      this._metricWidget.bind('selectedChanged', function(event,selected) { me._onMetricChanged(selected); event.preventDefault(); });
      
      this._xAxisWidget = $('.toolstrip.xaxis .xaxis-selector', this.element);
      this._xAxisWidget.openfit_popupselectmenu({selected:opts.xAxis});
      this._xAxisWidget.bind('selectedChanged', function(event,selected) { me._onXAxisChanged(selected); event.preventDefault(); });
      
      this.setDataTracks(opts.dataTracks);
    
      this._chartContainer = $('.chart-canvas-container:first', this.element);
      this._chartContainer.bind('plotselected', function (event, ranges) { me._onChartSelectionChanged(ranges.xaxis.from.toFixed(1), ranges.xaxis.to.toFixed(1)); });

      this._refreshChartData();
    },
    setDataTracks: function(dataTracks) {
      if (dataTracks == null) dataTracks = {};
      var opts = this.options;
      opts.dataTracks = dataTracks;
      this.setZoomRange(null);
      
      var metricItems = [];
      var firstMetricId = null;
      for (var trackId in dataTracks) {
        var track = dataTracks[trackId];
        track.id = track.type;
        var units = '';
        var m = track.measurement;
        if (m != null) {
          units = m.unit_symbol;
          m.f = m.conversion_factor;
          m.o = m.conversion_offset;
        }
        if (track.required != null) {
          var exists = false;
          for (var t in dataTracks) if (t == track.required) exists = true;
          if (!exists) continue;
        }
        if (track.hidden != null) continue;
        metricItems.push({id:track.id,name:track.title,units:units,measurement:m,color:track.color});
        if (firstMetricId == null) firstMetricId = trackId;
      }
      
      if (opts.metric.length == 0 && firstMetricId != null) opts.metric = firstMetricId;
      
      var xAxisItems = [];
      var units = '';
      if (opts.dataTracks['distance'] != null) {
        var m = opts.dataTracks['distance'].measurement;
        if (m != null) units = m.unit_symbol;
        xAxisItems.push({id:'distance',name:Drupal.t('Distance'),units:units});
      }
      xAxisItems.push({id:'time',name:Drupal.t('Time')});
      if(opts.xAxis.length == 0 && firstMetricId != null && xAxisItems.length > 0) opts.xAxis = xAxisItems[0].id;

      $('.toolstrip.metrics', this.element).toggle(firstMetricId != null);
      $('.toolstrip.settings', this.element).toggle(firstMetricId != null);
      $('.toolstrip.xaxis', this.element).toggle(firstMetricId != null);
      
      this.setMetricItems(metricItems,opts.metric);
      this.setXAxisItems(xAxisItems,opts.xAxis);
      this._refreshChartData();
    },
    convertZoomRange: function(newXAxis) {
      var z= this._zoomRange;
      var opts =  this.options;
      if (z != null && opts.xAxis != newXAxis && opts.dataTracks['distance'] != null) {
        var distanceTrack = opts.dataTracks['distance'];
        switch (opts.xAxis) {
          case 'time':
            if (newXAxis == 'distance') {
              this.setZoomRange([
                this._getValueAtTime(distanceTrack,z[0]),
                this._getValueAtTime(distanceTrack,z[1])
              ]);
            }
            break;
          case 'distance':
            if (newXAxis == 'time') {
              this.setZoomRange([
                this._getTimeAtDistance(z[0]),
                this._getTimeAtDistance(z[1])
              ]);
            }
            break;
        }
      }
    },
    options: {
      dataTracks: {},
      xAxis:'',
      metric:'',
      styles:{
        xaxis:{color:'rgba(0,0,0,.5)',tickColor:'rgba(0,0,0,0)'},
        yaxis:{color:'rgba(0,0,0,.7)',tickColor:'rgba(0,0,0,.2)'},
        line:{alpha:0.7},
        fill:{alpha:0.2}
      },
    },
    _refreshChartData: function() {
      if (this._chartContainer == null) return;
    
      var metricItem = this._getSelectedMetricItem();
      var metricId = metricItem != null ? metricItem.id : '';
      
      var me = this;
      var opts = this.options;
      var dataTrack = opts.dataTracks[metricId];
      
      try {    
        var chartData = this._getMetricData(metricItem);
      } catch(err) {
        var chartData = {data:null,min:0,max:2,avg:1};
      }
      // Spread the yaxis range out by 5% on each side
      var ymin = chartData.min;
      var ymax = chartData.max;
      var yavg = chartData.avg;
      var diff = Math.abs(ymax-ymin);
      
      if (diff < 0.01) {
        diff = 1;
        if (metricId == 'pace') diff = 30;
      } else {
        diff *= 0.05;
      }
      ymin = ymin >= 0 ? Math.max(0,ymin-diff) : ymin-diff;
      ymax = ymax <= 0 ? Math.min(0,ymax+diff) : ymax+diff;
      if (ymin == ymax && ymax == 0) ymax += 1;
      if (dataTrack != null && dataTrack.zoommaxavg != null) {
        ymax = Math.min(ymax,yavg*dataTrack.zoommaxavg);
      }
      
      var chartOptions = {
        legend:{show:false},
        shadowSize:0,
        grid:{borderWidth:0},
        xaxis:{color:opts.styles.xaxis.color,tickColor:opts.styles.xaxis.tickColor},
        yaxis:{color:opts.styles.yaxis.color,tickColor:opts.styles.yaxis.tickColor,ticks:4,min:ymin,max:ymax},
        selection:{mode:'x'}
      }
      
      var zoomRange = this._zoomRange;
      if (chartData.data != null && chartData.data.length == 0) {
        zoomRange = [0,1];
        chartOptions.xaxis.show = false;
        chartOptions.yaxis.labelWidth = 0;
      }
      if (zoomRange != null) {
        chartOptions.xaxis.min = zoomRange[0];
        chartOptions.xaxis.max = zoomRange[1];
      }
      switch (opts.xAxis) {
        case 'time':
          chartOptions.xaxis.tickFormatter = function(value,axis) {return me._tickFormatterTime(value,axis);};
          // TODO: Special tick generator for time
          break;
        case 'distance':
          var distanceTrack = opts.dataTracks['distance'];
          var measurement = distanceTrack != null ? distanceTrack.measurement : null;
          chartOptions.xaxis.tickFormatter = this._getTickFormatter(measurement);
          chartOptions.xaxis.ticks = this._getTickGenerator(measurement);
          break;
      }
      if (metricItem != null) {
        chartOptions.yaxis.tickFormatter = this._getTickFormatter(metricItem.measurement);
        chartOptions.yaxis.ticks = this._getTickGenerator(metricItem.measurement);
      }      
      
      var data = [];
      if (chartData.data != null) data = [chartData.data];
      $.plot(this._chartContainer, data, chartOptions);
    },
    _getMetricData: function(metricItem) {
      if (metricItem == null) return {data:null,min:0,max:2,avg:1};
      var opts = this.options;
      var dataTrack = opts.dataTracks[metricItem.id];
      if (dataTrack == null) return {data:null,min:0,max:2,avg:1};
      
      var dataTrackColor = dataTrack.color;
        // TODO: Cache data array for each track
      if (dataTrack['function'] != null) {
        var derived = this._getDerivedMetricData(dataTrack);
        var chartPoints = derived.chartPoints;
        var dataTrack = derived.dataTrack;
        if (chartPoints == null || dataTrack == null) return {data:null,min:0,max:2,avg:1};

      } else {
        var dataPoints = dataTrack.data;
        var numDataPoints = dataPoints != null ? dataPoints.length : 0;
        var numChartPoints = 2+numDataPoints;
        var lastChartPoint = numChartPoints-1;
        
        // Calculate y values from selected tack
        var chartPoints = new Array(numChartPoints);        
        chartPoints[0] = [null,dataTrack.start[1]];
        chartPoints[lastChartPoint] = [null,dataTrack.end[1]];
        for (var i = 0; i < numDataPoints; i++) chartPoints[i+1] = [null,dataPoints[i]];
      }
      var numDataPoints = dataTrack.data != null ? dataTrack.data.length : 0;
      var numChartPoints = chartPoints.length;
      var lastChartPoint = numChartPoints-1;

      // Calculate x values from time or distance track
      var interval = dataTrack.interval;
      var startInterval = Math.floor(dataTrack.start[0]/interval)+1;
      if (opts.xAxis == 'distance' && opts.dataTracks['distance'] != null) {
        var distanceTrack = opts.dataTracks['distance'];
        var numDistanceTrackPoints = distanceTrack.data.length;
        chartPoints[0][0] = this._getValueAtTime(distanceTrack,dataTrack.start[0]);
        chartPoints[lastChartPoint][0] = this._getValueAtTime(distanceTrack,dataTrack.end[0]);
        // TODO: If we are really paranoid we should assert 
        // distanceTrack.interval==dataTrack.interval and fall back to point interpolation
        // when intervals don't match. For now, the backend always returns identical intervals
        var distanceIntervalOffset = startInterval-Math.floor(distanceTrack.start[0]/distanceTrack.interval)+1;
        for (var i = 0; i < numDataPoints; i++) {
          var distanceIndex = distanceIntervalOffset+i;
          var distance;
          if (distanceIndex < 0) {
            distance = chartPoints[0][0];
          } else if (distanceIndex < numDistanceTrackPoints) {
            distance = distanceTrack.data[distanceIndex];
          } else {
            distance = chartPoints[lastChartPoint][0];
          }
          chartPoints[i+1][0] = distance;
        }
      } else {
        chartPoints[0][0] = dataTrack.start[0];
        chartPoints[lastChartPoint][0] = dataTrack.end[0];
        var time = startInterval*interval;
        for (var i = 0; i < numDataPoints; i++) {
          chartPoints[i+1][0] = time;
          time += interval;
        }
      }
      
      // Calculate min,max y values
      var min = metricItem.min != null ? metricItem.min : null;
      var max = metricItem.max != null ? metricItem.max : null;
      var pointInfo = this._calcMinMaxAvg(chartPoints,this._zoomRange,min,max);
      var trackRGB = this._parseColor(dataTrackColor);
      var lineColor = 'rgba(' + trackRGB.r + ',' + trackRGB.g + ',' + trackRGB.b + ',' + opts.styles.line.alpha + ')';
      var fillColor = 'rgba(' + trackRGB.r + ',' + trackRGB.g + ',' + trackRGB.b + ',' + opts.styles.fill.alpha + ')';
      var doFill = opts.styles.fill.alpha != 0;
      
      return {
        data:{color:lineColor,lines:{fill:doFill,fillColor:fillColor},data:chartPoints},
        min:pointInfo.min,max:pointInfo.max,avg:pointInfo.avg
      };
    },
    _getDerivedMetricData: function(dataTrack) {
      if (dataTrack['function'] == null) return {};
      return this[dataTrack['function']](dataTrack); 
    },
    _getSpeedTrack: function(dataTrack) {
      var track = this.options.dataTracks['distance'];
      var numDataPoints = track.data != null ? track.data.length : 0;
      var interval = track.interval;
      var startInterval = Math.floor(track.start[0]/interval)+1;
      var time = startInterval*interval;
      
      var points = new Array(numDataPoints+2);
      var empty = true;
      var prevPt = track.start;
      var p;
      var lookback = Math.ceil(10/interval); // 10 smoothing seconds
      for (var i = 0; i < numDataPoints; i++) {
        p = [time,track.data[i]];
        var speed = (p[0] > prevPt[0]) ? Math.max(0,(p[1]-prevPt[1])/(p[0]-prevPt[0])) : null;
        if (speed != null) speed = Math.round(speed*100)/100;
        var speedPt = [time,speed];
        if (empty) { points[0] = [track.start[0],speed]; empty = false; }
        points[i+1] = speedPt;
        var prevIndex = Math.max(0,i-lookback);
        prevPt = [time-(interval*(i-prevIndex)),track.data[prevIndex]];
        time += interval;
      }
      p = track.end;
      var speed = p[0] > prevPt[0] ? Math.max(0,(p[1]-prevPt[1])/(p[0]-prevPt[0])) : null;
      if (speed != null) speed = Math.round(speed*100)/100;
      var speedPt = [p[0],speed];
      if (empty) { points[0] = [track.start[0],speed]; empty = false; }
      points[numDataPoints+1] = speedPt;

      return {dataTrack:track,chartPoints:points};
    },
    _getPaceTrack: function(dataTrack) {
      var speedTrack = this._getSpeedTrack();
      var measurement = dataTrack.measurement;
      var points = speedTrack.chartPoints;
      var numPoints = points.length;
      for (var i = 0; i < numPoints; i++) {
        var speed = points[i][1];
        if (measurement != null) {
          if (measurement.f != 0 && measurement.f != 1) speed /= measurement.f;
          speed += measurement.o;
        }
        points[i][1] = speed == 0 ? null : Math.round(1/speed);
      }
      
      return {dataTrack:speedTrack.dataTrack,chartPoints:points};
    },
    _getValueAtTime: function(dataTrack, time) {
      if (time <= dataTrack.start[0]) return dataTrack.start[1];
      if (time >= dataTrack.end[0]) return dataTrack.end[1];
      
      var numDataPoints = dataTrack.data != null ? dataTrack.data.length : 0;
      var p1,p2;
      if (numDataPoints == 0) {
        p1 = dataTrack.start;
        p2 = dataTrack.end;
      } else {
        var interval = dataTrack.interval;
        var firstPointInterval = Math.floor(dataTrack.start[0]/interval)+1;
        var firstPointTime = firstPointInterval*interval;
        var lastPointTime = firstPointTime+((numDataPoints-1)*interval);
        if (time <= firstPointTime) { // Between start and first
          p1 = dataTrack.start;
          p2 = [firstPointTime,dataTrack.data[0]];
        } else if (time >= lastPointTime) { // Between last and end
          p1 = [lastPointTime,dataTrack.data[numDataPoints-1]];
          p2 = dataTrack.end;
        } else { // Between two points
          var intervalTime = time-firstPointTime;
          var i1 = Math.floor(intervalTime/interval);
          var i2 = Math.ceil(intervalTime/interval);
          p1 = [firstPointTime+i1*interval,dataTrack.data[i1]];
          p2 = [firstPointTime+i2*interval,dataTrack.data[i2]];
        }
      }
      return this._getInterpolatedValue(p1,p2,time);
    },
    _getTimeAtDistance: function(distance) {
      var dataTrack = this.options.dataTracks['distance'];
      if (dataTrack == null) return 0;
      
      if (distance <= dataTrack.start[1]) return dataTrack.start[0];
      if (distance >= dataTrack.end[1]) return dataTrack.end[0];
      
      var numDataPoints = dataTrack.data != null ? dataTrack.data.length : null;
      var p1,p2;
      if (numDataPoints == 0) {
        p1 = dataTrack.start;
        p2 = dataTrack.end;
      } else {
        var interval = dataTrack.interval;
        var firstPointInterval = Math.floor(dataTrack.start[0]/interval)+1;
        var firstPointTime = firstPointInterval*interval;
        var lastPointTime = firstPointTime+((numDataPoints-1)*interval);
        if (distance < dataTrack.data[0]) { // Between start and first
          p1 = dataTrack.start;
          p2 = [firstPointTime,dataTrack.data[0]];
        } else if (distance >= dataTrack.data[numDataPoints-1]) { // Between last and end
          p1 = [lastPointTime,dataTrack.data[numDataPoints-1]];
          p2 = dataTrack.end;
        } else { // Between two points
          var pos = this._binarySearchY(dataTrack.data,distance);
          if (pos == -1) return null;
          if (dataTrack.data[pos] == distance) {
            return firstPointTime+pos*interval;
          } else {
            var i1,i2;
            if (dataTrack.data[pos] > distance) {
              i1 = pos-1;
              i2 = pos;
            } else {
              i1 = pos;
              i2 = pos+1;
            }
            p1 = [firstPointTime+i1*interval,dataTrack.data[i1]];
            p2 = [firstPointTime+i2*interval,dataTrack.data[i2]];
          }
        }
      }
      p1 = [p1[1],p1[0]];
      p2 = [p2[1],p2[0]];
      return this._getInterpolatedValue(p1,p2,distance);
    },
    _binarySearchY: function(array,needle) {  
      if (!array.length) return -1;  

      var high = array.length - 1,low = 0;
      var mid, element;

      while (low <= high) {  
        mid = parseInt((low + high) / 2)  
        element = array[mid];  
        if (element > needle) {  
          high = mid - 1;  
        } else if (element < needle) {  
          low = mid + 1;  
        } else {  
          return mid;  
        }  
      }  

      return mid;  
    },
    _getInterpolatedValue: function(p1,p2,x) {
      if (p1[0] == p2[0] || p1[1] == p2[1]) return p1[1];
      if (x == p1[0]) return p1[1];
      if (x == p2[0]) return p2[1];
      var d = p2[0]-p1[0];
      var s = (p2[1]-p1[1])/d;
      return p1[1] + (x-p1[0])*s;
    },
  };
  $.widget("ui.openfit_activitydetailchart", $.ui.openfit_chartbase, ActivityDetailChart);
})(jQuery);
