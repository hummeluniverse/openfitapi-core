(function($) {
  var PopupSelectMenu = {
    _init: function() {
      var me = this
      this.setItems(this.options.items);
      this.setSelected(this.options.selected);
    },
    setItems: function(items) {
      this.options.items = items;
      $('.menu', this.element).remove();
      var menuHtml = '<div class="menu"><ul class="item-list">';
      if (this.options.items.length == 0) {
        menuHtml += '<li>&nbsp</li>';
      } else {
        $.each(this.options.items, function(index, value) {
          var itemHtml = value.html != null ? value.html : '<span class="name">' + value.name + '</span>';
          menuHtml += '<li><a href="#' + value.id + '" data-menuid="' + value.id + '">' + itemHtml + '</a></li>';
        });
      }
      menuHtml += '</ul></div>';
      $('.toolstrip-popup', this.element).append(menuHtml);
      var me = this;
      var menu = $('.menu', this.element);
      menu.each(function(index, value) {
        $('a', this).click(function(event) {
          var selectedId = $(this).attr('data-menuid');
          if (me.options.selected != selectedId) {
            me.setSelected(selectedId);
            me.element.trigger('selectedChanged', selectedId);
          }
          event.preventDefault();
        });
      });
      this.setSelected(this.options.selected);
      menu.css('minWidth', me.element.width());
    },    
    setSelected: function(selected) {
      var me = this;
      var menu = $('.menu', this.element);
      $.each(this.options.items, function(index, value) {
        if(value.id==selected) { 
          me.options.selected = selected;
          var itemText = value.html != null ? value.html : value.name;
          $('.btn-label', me.element).html(itemText);
          return false; 
        }
      });
      menu.css('minWidth', me.element.width());
    },    
    options: {
      items: [],
      selected: ''
    }
  };
  $.widget("ui.openfit_popupselectmenu", PopupSelectMenu);
})(jQuery);
