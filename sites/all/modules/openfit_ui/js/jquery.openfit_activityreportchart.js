(function($) {
  var ActivityReportChart = {
    _init: function() {
      var me = this;
      var opts = this.options;
      
      this._zoomOutButton = $('.zoom-out-button', this.element);
      this._zoomOutButton.click(function(event) { me._onZoomOutClicked(); event.preventDefault(); });
    
      this._metricWidget = $('.toolstrip.metrics .chart-metric', this.element);
      this._metricWidget.openfit_popupselectmenu();
      this._metricWidget.bind('selectedChanged', function(event,selected) { me._onMetricChanged(selected); event.preventDefault(); });
      
      this._xAxisWidget = $('.toolstrip.xaxis .xaxis-selector', this.element);
      this._xAxisWidget.openfit_popupselectmenu({selected:opts.xAxis});
      this._xAxisWidget.bind('selectedChanged', function(event,selected) { me._onXAxisChanged(selected); event.preventDefault(); });
      
      this._chartContainer = $('.chart-canvas-container:first', this.element);
      this._chartContainer.bind('plotselected', function (event, ranges) { me._onChartSelectionChanged(ranges.xaxis.from.toFixed(1), ranges.xaxis.to.toFixed(1)); });

      this._refreshChartData();
    },
    setData: function(data) {
      this.options.data = data;
      this._refreshChartData();
    },
    convertZoomRange: function(newXAxis) {
      this.setZoomRange(null);
    },
    options: {
      data: {},
      xAxis:'',
      metric:'',
      metricItems:[]
    },
    _refreshChartData: function() {
      if (this._chartContainer == null) return;
    
      var metricItem = this._getSelectedMetricItem();
      try {    
        var chartData = this._getMetricData(metricItem);
      } catch(err) {
        console.log(err);
        var chartData = {data:null,min:0,max:2,avg:1,labels:[]};
      }
      // Spread the yaxis range out by 5% on each side
      var ymin = chartData.min;
      var ymax = chartData.max;
      var yavg = chartData.avg;
      var diff = Math.abs(ymax-ymin);
      
      if (diff < 0.01) {
        diff = 1;
        if (metricItem.id == 'pace') diff = 30;
      } else {
        diff *= 0.05;
      }
      ymin = ymin >= 0 ? Math.max(0,ymin-diff) : ymin-diff;
      ymax = ymax <= 0 ? Math.min(0,ymax+diff) : ymax+diff;
      if (ymin == ymax && ymax == 0) ymax += 1;
      // TODO: Might need this when showing pace - or maybe not because variation is smaller.
      //if (dataTrack != null && dataTrack.zoommaxavg != null) {
      //  ymax = Math.min(ymax,yavg*dataTrack.zoommaxavg);
      //}
      
      var chartOptions = {
        legend:{show:false},
        shadowSize:0,
        grid:{borderWidth:0},
        xaxis:{color:'#AAA',tickColor:'rgba(0,0,0,0)'},
        yaxis:{color:'#666',tickColor:'#EEE',ticks:4,min:ymin,max:ymax},
        selection:{mode:'x'}
      }
      
      var zoomRange = this._zoomRange;
      if (chartData.data != null && chartData.data.length == 0) {
        zoomRange = [0,1];
        chartOptions.xaxis.show = false;
        chartOptions.yaxis.labelWidth = 0;
      }
      if (zoomRange != null) {
        chartOptions.xaxis.min = zoomRange[0];
        chartOptions.xaxis.max = zoomRange[1];
      }
      var xAxisTicks = [];
      var numLabels = chartData.labels.length;
      for(var i = 0; i < numLabels; i++) xAxisTicks.push([i+0.4,chartData.labels[i]]);
      chartOptions.xaxis.ticks = xAxisTicks;
      
      if (metricItem != null) {
        chartOptions.yaxis.tickFormatter = this._getTickFormatter(metricItem.measurement);
        chartOptions.yaxis.ticks = this._getTickGenerator(metricItem.measurement);
      }
      
      var data = [];
      if (chartData.data != null) data = [chartData.data];
      $.plot(this._chartContainer, data, chartOptions);
    },
    _getPaceData: function(metricItem,data) {
      var paceData = [];
      var speed = data != null ? data.data['speed'] : null;
      if (speed != null) {
        var measurement = metricItem != null ? metricItem.measurement : null;
        var numSpeed = speed.length;
        for (var i = 0; i < numSpeed; i++) {
          var pace = null;
          if (speed[i] != null && speed[i] > 0) {
            pace = 1/speed[i];
            if (measurement != null) {
              if (measurement.f != 0 && measurement.f != 1) pace *= measurement.f;
              pace -= measurement.o;
            }
            pace = Math.round(pace);
          }
          paceData.push(pace);
        }
      }
      return paceData;
    },
    _getMetricData: function(metricItem) {
      if (metricItem == null) return {data:null,min:0,max:2,avg:1,labels:[]};
      var opts = this.options;
      var seriesColor = '#9BC642';
      
      var data = opts.data[opts.xAxis];
      var labels = [];
      if (data != null) {
        labels = data.labels;
        if (metricItem['function'] != null) {
          data = this[metricItem['function']](metricItem,data);
        } else {
          data = data.data[metricItem.id];
        }
      }
      if (data == null || data.length == 0) return {data:null,min:0,max:2,avg:1,labels:[]};
      
      var numChartPoints = data.length;
      var chartPoints = new Array(numChartPoints);
      for (var i = 0; i < numChartPoints; i++) {
        chartPoints[i] = [i,data[i]];
      }
      // Calculate min,max y values
      var min = metricItem.min != null ? metricItem.min : null;
      var max = metricItem.max != null ? metricItem.max : null;
      var pointInfo = this._calcMinMaxAvg(chartPoints,this._zoomRange,min,max);
      
      var barWidthPixel = this._chartContainer.width()/(numChartPoints);
      var barWidth = Math.min(0.97,Math.max(0.8,(barWidthPixel-8)/barWidthPixel));

      var fillGradientLightColor = this._changeColor(seriesColor, 0.3, false);
      var strokeColor = this._changeColor(seriesColor, 0.2, true);
      return {
        data:{color:strokeColor,
          bars:{show:true,fill:true,
            fillColor:{colors:[fillGradientLightColor,seriesColor]},
            barWidth:barWidth,lineWidth:0.5,radius:4
          },
          data:chartPoints
        },
        min:pointInfo.min,max:pointInfo.max,avg:pointInfo.avg,labels:labels
      };
    },
  };
  $.widget("ui.openfit_activityreportchart", $.ui.openfit_chartbase, ActivityReportChart);
})(jQuery);
