(function($) {
  $(document).ready(function() {
    // Add document listener to close any popups.
    $(document).click(function(event) {      
      var target = $(event.target).closest('.toolstrip-button');
      var mymenu = null;
      if (target.parent('.toolstrip')) mymenu = target.children('.toolstrip-popup-wrap')[0];
      
      $('.toolstrip .toolstrip-popup-wrap').each(function(index) {
        if (this != mymenu) {
          var othermenu = $(this);
          othermenu.hide();
          othermenu.parent().removeClass('popup-open');
        }
      });
    });
  });
})(jQuery);