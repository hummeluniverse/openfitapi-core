<?php $activity_url = drupal_get_path_alias('node/' . $node->nid); ?>
<article id="activity-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php
  echo $user_picture;
  $markup = drupal_render($content);
  $markup = "<div$content_attributes>$markup</div>";
  $url = drupal_get_path_alias('node/' . $node->nid);
  print l($markup, $url, array('alias' => TRUE, 'html' => TRUE));
?>
</article>
